<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrasi', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('asalinstansi');
            $table->string('nohp');
            $table->string('email');
            $table->biginteger('idkeperluan');
            $table->string('fotodiri');
            $table->string('fotoktp');
            $table->UnsignedBigInteger('idpic')->nullable();
           // $table->foreign('idpic')->references('id')->on('pic');
            $table->string('signoffpic')->nullable();
            $table->datetime('checkintime');
            $table->datetime('checkouttime')->nullable();
            $table->string('cardid')->nullable();
            $table->string('cardidstatus')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrasi');
    }
}
