<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/demo',  function  (Request $request)  {
    return response()->json(['Laravel 8 CORS Demo']);
 });

Route::get('/viewkeperluan','\App\Http\Controllers\keperluanController@view');


    Route::post('/createregistrasiselfie','\App\Http\Controllers\registrasiController@createselfie');
    Route::post('/createregistrasiktp','\App\Http\Controllers\registrasiController@createktp');
    Route::post('/createregistrasi','\App\Http\Controllers\registrasiController@create');
    Route::post('/checkoutregistrasi','\App\Http\Controllers\registrasiController@checkout');
    Route::post('/updatepicregistrasi','\App\Http\Controllers\registrasiController@updatepic');
    Route::post('/addnotulen','\App\Http\Controllers\notulenController@add');
    Route::get('/viewkeperluan','\App\Http\Controllers\keperluanController@view');
