# Endpoints


## api/user




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETapi-user" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-user"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-user"></code></pre>
</div>
<div id="execution-error-GETapi-user" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-user"></code></pre>
</div>
<form id="form-GETapi-user" data-method="GET" data-path="api/user" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-user', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-user" onclick="tryItOut('GETapi-user');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-user" onclick="cancelTryOut('GETapi-user');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-user" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/user</code></b>
</p>
</form>


## /




> Example request:

```bash
curl -X GET \
    -G "http://localhost/" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'Nunito';
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
            
            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
                    <svg viewBox="0 0 651 192" fill="none" xmlns="http://www.w3.org/2000/svg" class="h-16 w-auto text-gray-700 sm:h-20">
                        <g clip-path="url(#clip0)" fill="#EF3B2D">
                            <path d="M248.032 44.676h-16.466v100.23h47.394v-14.748h-30.928V44.676zM337.091 87.202c-2.101-3.341-5.083-5.965-8.949-7.875-3.865-1.909-7.756-2.864-11.669-2.864-5.062 0-9.69.931-13.89 2.792-4.201 1.861-7.804 4.417-10.811 7.661-3.007 3.246-5.347 6.993-7.016 11.239-1.672 4.249-2.506 8.713-2.506 13.389 0 4.774.834 9.26 2.506 13.459 1.669 4.202 4.009 7.925 7.016 11.169 3.007 3.246 6.609 5.799 10.811 7.66 4.199 1.861 8.828 2.792 13.89 2.792 3.913 0 7.804-.955 11.669-2.863 3.866-1.908 6.849-4.533 8.949-7.875v9.021h15.607V78.182h-15.607v9.02zm-1.431 32.503c-.955 2.578-2.291 4.821-4.009 6.73-1.719 1.91-3.795 3.437-6.229 4.582-2.435 1.146-5.133 1.718-8.091 1.718-2.96 0-5.633-.572-8.019-1.718-2.387-1.146-4.438-2.672-6.156-4.582-1.719-1.909-3.032-4.152-3.938-6.73-.909-2.577-1.36-5.298-1.36-8.161 0-2.864.451-5.585 1.36-8.162.905-2.577 2.219-4.819 3.938-6.729 1.718-1.908 3.77-3.437 6.156-4.582 2.386-1.146 5.059-1.718 8.019-1.718 2.958 0 5.656.572 8.091 1.718 2.434 1.146 4.51 2.674 6.229 4.582 1.718 1.91 3.054 4.152 4.009 6.729.953 2.577 1.432 5.298 1.432 8.162-.001 2.863-.479 5.584-1.432 8.161zM463.954 87.202c-2.101-3.341-5.083-5.965-8.949-7.875-3.865-1.909-7.756-2.864-11.669-2.864-5.062 0-9.69.931-13.89 2.792-4.201 1.861-7.804 4.417-10.811 7.661-3.007 3.246-5.347 6.993-7.016 11.239-1.672 4.249-2.506 8.713-2.506 13.389 0 4.774.834 9.26 2.506 13.459 1.669 4.202 4.009 7.925 7.016 11.169 3.007 3.246 6.609 5.799 10.811 7.66 4.199 1.861 8.828 2.792 13.89 2.792 3.913 0 7.804-.955 11.669-2.863 3.866-1.908 6.849-4.533 8.949-7.875v9.021h15.607V78.182h-15.607v9.02zm-1.432 32.503c-.955 2.578-2.291 4.821-4.009 6.73-1.719 1.91-3.795 3.437-6.229 4.582-2.435 1.146-5.133 1.718-8.091 1.718-2.96 0-5.633-.572-8.019-1.718-2.387-1.146-4.438-2.672-6.156-4.582-1.719-1.909-3.032-4.152-3.938-6.73-.909-2.577-1.36-5.298-1.36-8.161 0-2.864.451-5.585 1.36-8.162.905-2.577 2.219-4.819 3.938-6.729 1.718-1.908 3.77-3.437 6.156-4.582 2.386-1.146 5.059-1.718 8.019-1.718 2.958 0 5.656.572 8.091 1.718 2.434 1.146 4.51 2.674 6.229 4.582 1.718 1.91 3.054 4.152 4.009 6.729.953 2.577 1.432 5.298 1.432 8.162 0 2.863-.479 5.584-1.432 8.161zM650.772 44.676h-15.606v100.23h15.606V44.676zM365.013 144.906h15.607V93.538h26.776V78.182h-42.383v66.724zM542.133 78.182l-19.616 51.096-19.616-51.096h-15.808l25.617 66.724h19.614l25.617-66.724h-15.808zM591.98 76.466c-19.112 0-34.239 15.706-34.239 35.079 0 21.416 14.641 35.079 36.239 35.079 12.088 0 19.806-4.622 29.234-14.688l-10.544-8.158c-.006.008-7.958 10.449-19.832 10.449-13.802 0-19.612-11.127-19.612-16.884h51.777c2.72-22.043-11.772-40.877-33.023-40.877zm-18.713 29.28c.12-1.284 1.917-16.884 18.589-16.884 16.671 0 18.697 15.598 18.813 16.884h-37.402zM184.068 43.892c-.024-.088-.073-.165-.104-.25-.058-.157-.108-.316-.191-.46-.056-.097-.137-.176-.203-.265-.087-.117-.161-.242-.265-.345-.085-.086-.194-.148-.29-.223-.109-.085-.206-.182-.327-.252l-.002-.001-.002-.002-35.648-20.524a2.971 2.971 0 00-2.964 0l-35.647 20.522-.002.002-.002.001c-.121.07-.219.167-.327.252-.096.075-.205.138-.29.223-.103.103-.178.228-.265.345-.066.089-.147.169-.203.265-.083.144-.133.304-.191.46-.031.085-.08.162-.104.25-.067.249-.103.51-.103.776v38.979l-29.706 17.103V24.493a3 3 0 00-.103-.776c-.024-.088-.073-.165-.104-.25-.058-.157-.108-.316-.191-.46-.056-.097-.137-.176-.203-.265-.087-.117-.161-.242-.265-.345-.085-.086-.194-.148-.29-.223-.109-.085-.206-.182-.327-.252l-.002-.001-.002-.002L40.098 1.396a2.971 2.971 0 00-2.964 0L1.487 21.919l-.002.002-.002.001c-.121.07-.219.167-.327.252-.096.075-.205.138-.29.223-.103.103-.178.228-.265.345-.066.089-.147.169-.203.265-.083.144-.133.304-.191.46-.031.085-.08.162-.104.25-.067.249-.103.51-.103.776v122.09c0 1.063.568 2.044 1.489 2.575l71.293 41.045c.156.089.324.143.49.202.078.028.15.074.23.095a2.98 2.98 0 001.524 0c.069-.018.132-.059.2-.083.176-.061.354-.119.519-.214l71.293-41.045a2.971 2.971 0 001.489-2.575v-38.979l34.158-19.666a2.971 2.971 0 001.489-2.575V44.666a3.075 3.075 0 00-.106-.774zM74.255 143.167l-29.648-16.779 31.136-17.926.001-.001 34.164-19.669 29.674 17.084-21.772 12.428-43.555 24.863zm68.329-76.259v33.841l-12.475-7.182-17.231-9.92V49.806l12.475 7.182 17.231 9.92zm2.97-39.335l29.693 17.095-29.693 17.095-29.693-17.095 29.693-17.095zM54.06 114.089l-12.475 7.182V46.733l17.231-9.92 12.475-7.182v74.537l-17.231 9.921zM38.614 7.398l29.693 17.095-29.693 17.095L8.921 24.493 38.614 7.398zM5.938 29.632l12.475 7.182 17.231 9.92v79.676l.001.005-.001.006c0 .114.032.221.045.333.017.146.021.294.059.434l.002.007c.032.117.094.222.14.334.051.124.088.255.156.371a.036.036 0 00.004.009c.061.105.149.191.222.288.081.105.149.22.244.314l.008.01c.084.083.19.142.284.215.106.083.202.178.32.247l.013.005.011.008 34.139 19.321v34.175L5.939 144.867V29.632h-.001zm136.646 115.235l-65.352 37.625V148.31l48.399-27.628 16.953-9.677v33.862zm35.646-61.22l-29.706 17.102V66.908l17.231-9.92 12.475-7.182v33.841z"/>
                        </g>
                    </svg>
                </div>

                <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <div class="grid grid-cols-1 md:grid-cols-2">
                        <div class="p-6">
                            <div class="flex items-center">
                                <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-500"><path d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"></path></svg>
                                <div class="ml-4 text-lg leading-7 font-semibold"><a href="https://laravel.com/docs" class="underline text-gray-900 dark:text-white">Documentation</a></div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    Laravel has wonderful, thorough documentation covering every aspect of the framework. Whether you are new to the framework or have previous experience with Laravel, we recommend reading all of the documentation from beginning to end.
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                            <div class="flex items-center">
                                <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-500"><path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path><path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>
                                <div class="ml-4 text-lg leading-7 font-semibold"><a href="https://laracasts.com" class="underline text-gray-900 dark:text-white">Laracasts</a></div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    Laracasts offers thousands of video tutorials on Laravel, PHP, and JavaScript development. Check them out, see for yourself, and massively level up your development skills in the process.
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700">
                            <div class="flex items-center">
                                <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-500"><path d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"></path></svg>
                                <div class="ml-4 text-lg leading-7 font-semibold"><a href="https://laravel-news.com/" class="underline text-gray-900 dark:text-white">Laravel News</a></div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    Laravel News is a community driven portal and newsletter aggregating all of the latest and most important news in the Laravel ecosystem, including new package releases and tutorials.
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-l">
                            <div class="flex items-center">
                                <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-500"><path d="M3.055 11H5a2 2 0 012 2v1a2 2 0 002 2 2 2 0 012 2v2.945M8 3.935V5.5A2.5 2.5 0 0010.5 8h.5a2 2 0 012 2 2 2 0 104 0 2 2 0 012-2h1.064M15 20.488V18a2 2 0 012-2h3.064M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                                <div class="ml-4 text-lg leading-7 font-semibold text-gray-900 dark:text-white">Vibrant Ecosystem</div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                    Laravel's robust library of first-party tools and libraries, such as <a href="https://forge.laravel.com" class="underline">Forge</a>, <a href="https://vapor.laravel.com" class="underline">Vapor</a>, <a href="https://nova.laravel.com" class="underline">Nova</a>, and <a href="https://envoyer.io" class="underline">Envoyer</a> help you take your projects to the next level. Pair them with powerful open source libraries like <a href="https://laravel.com/docs/billing" class="underline">Cashier</a>, <a href="https://laravel.com/docs/dusk" class="underline">Dusk</a>, <a href="https://laravel.com/docs/broadcasting" class="underline">Echo</a>, <a href="https://laravel.com/docs/horizon" class="underline">Horizon</a>, <a href="https://laravel.com/docs/sanctum" class="underline">Sanctum</a>, <a href="https://laravel.com/docs/telescope" class="underline">Telescope</a>, and more.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex justify-center mt-4 sm:items-center sm:justify-between">
                    <div class="text-center text-sm text-gray-500 sm:text-left">
                        <div class="flex items-center">
                            <svg fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor" class="-mt-px w-5 h-5 text-gray-400">
                                <path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
                            </svg>

                            <a href="https://laravel.bigcartel.com" class="ml-1 underline">
                                Shop
                            </a>

                            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="ml-4 -mt-px w-5 h-5 text-gray-400">
                                <path d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"></path>
                            </svg>

                            <a href="https://github.com/sponsors/taylorotwell" class="ml-1 underline">
                                Sponsor
                            </a>
                        </div>
                    </div>

                    <div class="ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">
                        Laravel v8.26.1 (PHP v7.4.14)
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

```
<div id="execution-results-GET-" hidden>
    <blockquote>Received response<span id="execution-response-status-GET-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GET-"></code></pre>
</div>
<div id="execution-error-GET-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GET-"></code></pre>
</div>
<form id="form-GET-" data-method="GET" data-path="/" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GET-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GET-" onclick="tryItOut('GET-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GET-" onclick="cancelTryOut('GET-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GET-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>/</code></b>
</p>
</form>


## Show the application&#039;s login form.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json

<!DOCTYPE html>
<html lang="en" class="h-full font-sans antialiased">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="QDDXQdwZ7q0GV6J4jllefz6Q6xQsmlEfA3eT4Lf3">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,800,800i,900,900i" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="/vendor/nova/app.css?id=40717efbdd839ce5aad5">

    <!-- Custom Meta Data -->
    
    <!-- Theme Styles -->
    </head>
<body class="bg-40 text-black h-full">
    <div class="h-full">
        <div class="px-view py-view mx-auto">
            
<div class="mx-auto py-8 max-w-sm text-center text-90">
    <svg
    class="fill-current"
    width="200"
    height="39"
    viewBox="0 0 126 24"
    xmlns="http://www.w3.org/2000/svg"
>
    <path d="M40.76 18h-6.8V7.328h2.288V16h4.512v2zm8.064 0h-2.048v-.816c-.528.64-1.44 1.008-2.448 1.008-1.232 0-2.672-.832-2.672-2.56 0-1.824 1.44-2.496 2.672-2.496 1.04 0 1.936.336 2.448.944v-.976c0-.784-.672-1.296-1.696-1.296-.816 0-1.584.32-2.224.912l-.8-1.424c.944-.848 2.16-1.216 3.376-1.216 1.776 0 3.392.704 3.392 2.928V18zm-3.68-1.184c.656 0 1.296-.224 1.632-.672v-.96c-.336-.448-.976-.688-1.632-.688-.8 0-1.456.432-1.456 1.168s.656 1.152 1.456 1.152zM52.856 18h-2.032v-7.728h2.032v1.04c.56-.672 1.504-1.232 2.464-1.232v1.984a2.595 2.595 0 0 0-.56-.048c-.672 0-1.568.384-1.904.88V18zm10.416 0h-2.048v-.816c-.528.64-1.44 1.008-2.448 1.008-1.232 0-2.672-.832-2.672-2.56 0-1.824 1.44-2.496 2.672-2.496 1.04 0 1.936.336 2.448.944v-.976c0-.784-.672-1.296-1.696-1.296-.816 0-1.584.32-2.224.912l-.8-1.424c.944-.848 2.16-1.216 3.376-1.216 1.776 0 3.392.704 3.392 2.928V18zm-3.68-1.184c.656 0 1.296-.224 1.632-.672v-.96c-.336-.448-.976-.688-1.632-.688-.8 0-1.456.432-1.456 1.168s.656 1.152 1.456 1.152zM69.464 18h-2.192l-3.104-7.728h2.176l2.016 5.376 2.032-5.376h2.176L69.464 18zm7.648.192c-2.352 0-4.128-1.584-4.128-4.064 0-2.24 1.664-4.048 4-4.048 2.32 0 3.872 1.728 3.872 4.24v.48h-5.744c.144.944.912 1.728 2.224 1.728.656 0 1.552-.272 2.048-.752l.912 1.344c-.768.704-1.984 1.072-3.184 1.072zm1.792-4.8c-.064-.736-.576-1.648-1.92-1.648-1.264 0-1.808.88-1.888 1.648h3.808zM84.36 18h-2.032V7.328h2.032V18zm15.232 0h-1.28l-6.224-8.512V18H90.76V7.328h1.36l6.144 8.336V7.328h1.328V18zm5.824.192c-2.352 0-3.824-1.824-3.824-4.064s1.472-4.048 3.824-4.048 3.824 1.808 3.824 4.048-1.472 4.064-3.824 4.064zm0-1.072c1.648 0 2.56-1.408 2.56-2.992 0-1.568-.912-2.976-2.56-2.976-1.648 0-2.56 1.408-2.56 2.976 0 1.584.912 2.992 2.56 2.992zm9.152.88h-1.312l-3.216-7.728h1.312l2.56 6.336 2.576-6.336h1.296L114.568 18zm10.496 0h-1.2v-.88c-.624.704-1.52 1.072-2.56 1.072-1.296 0-2.688-.88-2.688-2.56 0-1.744 1.376-2.544 2.688-2.544 1.056 0 1.936.336 2.56 1.04v-1.392c0-1.024-.832-1.616-1.952-1.616-.928 0-1.68.32-2.368 1.072l-.56-.832c.832-.864 1.824-1.28 3.088-1.28 1.648 0 2.992.736 2.992 2.608V18zm-3.312-.672c.832 0 1.648-.32 2.112-.96v-1.472c-.464-.624-1.28-.944-2.112-.944-1.136 0-1.92.704-1.92 1.68 0 .992.784 1.696 1.92 1.696zM20.119 20.455A12.184 12.184 0 0 1 11.5 24a12.18 12.18 0 0 1-9.333-4.319c4.772 3.933 11.88 3.687 16.36-.738a7.571 7.571 0 0 0 0-10.8c-3.018-2.982-7.912-2.982-10.931 0a3.245 3.245 0 0 0 0 4.628 3.342 3.342 0 0 0 4.685 0 1.114 1.114 0 0 1 1.561 0 1.082 1.082 0 0 1 0 1.543 5.57 5.57 0 0 1-7.808 0 5.408 5.408 0 0 1 0-7.714c3.881-3.834 10.174-3.834 14.055 0a9.734 9.734 0 0 1 .03 13.855zm.714-16.136C16.06.386 8.953.632 4.473 5.057a7.571 7.571 0 0 0 0 10.8c3.018 2.982 7.912 2.982 10.931 0a3.245 3.245 0 0 0 0-4.628 3.342 3.342 0 0 0-4.685 0 1.114 1.114 0 0 1-1.561 0 1.082 1.082 0 0 1 0-1.543 5.57 5.57 0 0 1 7.808 0 5.408 5.408 0 0 1 0 7.714c-3.881 3.834-10.174 3.834-14.055 0a9.734 9.734 0 0 1-.015-13.87C5.096 1.35 8.138 0 11.5 0c3.75 0 7.105 1.68 9.333 4.319z" fill-rule="evenodd"/>
</svg>
</div>

<form
    class="bg-white shadow rounded-lg p-8 max-w-login mx-auto"
    method="POST"
    action="http://localhost/nova/login"
>
    <input type="hidden" name="_token" value="QDDXQdwZ7q0GV6J4jllefz6Q6xQsmlEfA3eT4Lf3">

    <h2 class="text-2xl text-center font-normal mb-6 text-90">Welcome Back!</h2>
<svg class="block mx-auto mb-6" xmlns="http://www.w3.org/2000/svg" width="100" height="2" viewBox="0 0 100 2">
  <path fill="#D8E3EC" d="M0 0h100v2H0z"/>
</svg>

    
    <div class="mb-6 ">
        <label class="block font-bold mb-2" for="email">Email Address</label>
        <input class="form-control form-input form-input-bordered w-full" id="email" type="email" name="email" value="" required autofocus>
    </div>

    <div class="mb-6 ">
        <label class="block font-bold mb-2" for="password">Password</label>
        <input class="form-control form-input form-input-bordered w-full" id="password" type="password" name="password" required>
    </div>

    <div class="flex mb-6">
        <label class="flex items-center block text-xl font-bold">
            <input class="" type="checkbox" name="remember" >
            <span class="text-base ml-2">Remember Me</span>
        </label>


                <div class="ml-auto">
            <a class="text-primary dim font-bold no-underline" href="http://localhost/nova/password/reset">
                Forgot Your Password?
            </a>
        </div>
            </div>

    <button class="w-full btn btn-default btn-primary hover:bg-primary-dark" type="submit">
        Login
    </button>
</form>
        </div>
    </div>
</body>
</html>

```
<div id="execution-results-GETnova-login" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-login"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-login"></code></pre>
</div>
<div id="execution-error-GETnova-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-login"></code></pre>
</div>
<form id="form-GETnova-login" data-method="GET" data-path="nova/login" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-login', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-login" onclick="tryItOut('GETnova-login');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-login" onclick="cancelTryOut('GETnova-login');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-login" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova/login</code></b>
</p>
</form>


## Handle a login request to the application.




> Example request:

```bash
curl -X POST \
    "http://localhost/nova/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTnova-login" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTnova-login"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTnova-login"></code></pre>
</div>
<div id="execution-error-POSTnova-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTnova-login"></code></pre>
</div>
<form id="form-POSTnova-login" data-method="POST" data-path="nova/login" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTnova-login', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTnova-login" onclick="tryItOut('POSTnova-login');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTnova-login" onclick="cancelTryOut('POSTnova-login');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTnova-login" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>nova/login</code></b>
</p>
</form>


## Log the user out of the application.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-logout" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-logout"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-logout"></code></pre>
</div>
<div id="execution-error-GETnova-logout" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-logout"></code></pre>
</div>
<form id="form-GETnova-logout" data-method="GET" data-path="nova/logout" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-logout', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-logout" onclick="tryItOut('GETnova-logout');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-logout" onclick="cancelTryOut('GETnova-logout');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-logout" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova/logout</code></b>
</p>
</form>


## Display the form to request a password reset link.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova/password/reset" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova/password/reset"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json

<!DOCTYPE html>
<html lang="en" class="h-full font-sans antialiased">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="QDDXQdwZ7q0GV6J4jllefz6Q6xQsmlEfA3eT4Lf3">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,800,800i,900,900i" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="/vendor/nova/app.css?id=40717efbdd839ce5aad5">

    <!-- Custom Meta Data -->
    
    <!-- Theme Styles -->
    </head>
<body class="bg-40 text-black h-full">
    <div class="h-full">
        <div class="px-view py-view mx-auto">
            
<div class="mx-auto py-8 max-w-sm text-center text-90">
    <svg
    class="fill-current"
    width="200"
    height="39"
    viewBox="0 0 126 24"
    xmlns="http://www.w3.org/2000/svg"
>
    <path d="M40.76 18h-6.8V7.328h2.288V16h4.512v2zm8.064 0h-2.048v-.816c-.528.64-1.44 1.008-2.448 1.008-1.232 0-2.672-.832-2.672-2.56 0-1.824 1.44-2.496 2.672-2.496 1.04 0 1.936.336 2.448.944v-.976c0-.784-.672-1.296-1.696-1.296-.816 0-1.584.32-2.224.912l-.8-1.424c.944-.848 2.16-1.216 3.376-1.216 1.776 0 3.392.704 3.392 2.928V18zm-3.68-1.184c.656 0 1.296-.224 1.632-.672v-.96c-.336-.448-.976-.688-1.632-.688-.8 0-1.456.432-1.456 1.168s.656 1.152 1.456 1.152zM52.856 18h-2.032v-7.728h2.032v1.04c.56-.672 1.504-1.232 2.464-1.232v1.984a2.595 2.595 0 0 0-.56-.048c-.672 0-1.568.384-1.904.88V18zm10.416 0h-2.048v-.816c-.528.64-1.44 1.008-2.448 1.008-1.232 0-2.672-.832-2.672-2.56 0-1.824 1.44-2.496 2.672-2.496 1.04 0 1.936.336 2.448.944v-.976c0-.784-.672-1.296-1.696-1.296-.816 0-1.584.32-2.224.912l-.8-1.424c.944-.848 2.16-1.216 3.376-1.216 1.776 0 3.392.704 3.392 2.928V18zm-3.68-1.184c.656 0 1.296-.224 1.632-.672v-.96c-.336-.448-.976-.688-1.632-.688-.8 0-1.456.432-1.456 1.168s.656 1.152 1.456 1.152zM69.464 18h-2.192l-3.104-7.728h2.176l2.016 5.376 2.032-5.376h2.176L69.464 18zm7.648.192c-2.352 0-4.128-1.584-4.128-4.064 0-2.24 1.664-4.048 4-4.048 2.32 0 3.872 1.728 3.872 4.24v.48h-5.744c.144.944.912 1.728 2.224 1.728.656 0 1.552-.272 2.048-.752l.912 1.344c-.768.704-1.984 1.072-3.184 1.072zm1.792-4.8c-.064-.736-.576-1.648-1.92-1.648-1.264 0-1.808.88-1.888 1.648h3.808zM84.36 18h-2.032V7.328h2.032V18zm15.232 0h-1.28l-6.224-8.512V18H90.76V7.328h1.36l6.144 8.336V7.328h1.328V18zm5.824.192c-2.352 0-3.824-1.824-3.824-4.064s1.472-4.048 3.824-4.048 3.824 1.808 3.824 4.048-1.472 4.064-3.824 4.064zm0-1.072c1.648 0 2.56-1.408 2.56-2.992 0-1.568-.912-2.976-2.56-2.976-1.648 0-2.56 1.408-2.56 2.976 0 1.584.912 2.992 2.56 2.992zm9.152.88h-1.312l-3.216-7.728h1.312l2.56 6.336 2.576-6.336h1.296L114.568 18zm10.496 0h-1.2v-.88c-.624.704-1.52 1.072-2.56 1.072-1.296 0-2.688-.88-2.688-2.56 0-1.744 1.376-2.544 2.688-2.544 1.056 0 1.936.336 2.56 1.04v-1.392c0-1.024-.832-1.616-1.952-1.616-.928 0-1.68.32-2.368 1.072l-.56-.832c.832-.864 1.824-1.28 3.088-1.28 1.648 0 2.992.736 2.992 2.608V18zm-3.312-.672c.832 0 1.648-.32 2.112-.96v-1.472c-.464-.624-1.28-.944-2.112-.944-1.136 0-1.92.704-1.92 1.68 0 .992.784 1.696 1.92 1.696zM20.119 20.455A12.184 12.184 0 0 1 11.5 24a12.18 12.18 0 0 1-9.333-4.319c4.772 3.933 11.88 3.687 16.36-.738a7.571 7.571 0 0 0 0-10.8c-3.018-2.982-7.912-2.982-10.931 0a3.245 3.245 0 0 0 0 4.628 3.342 3.342 0 0 0 4.685 0 1.114 1.114 0 0 1 1.561 0 1.082 1.082 0 0 1 0 1.543 5.57 5.57 0 0 1-7.808 0 5.408 5.408 0 0 1 0-7.714c3.881-3.834 10.174-3.834 14.055 0a9.734 9.734 0 0 1 .03 13.855zm.714-16.136C16.06.386 8.953.632 4.473 5.057a7.571 7.571 0 0 0 0 10.8c3.018 2.982 7.912 2.982 10.931 0a3.245 3.245 0 0 0 0-4.628 3.342 3.342 0 0 0-4.685 0 1.114 1.114 0 0 1-1.561 0 1.082 1.082 0 0 1 0-1.543 5.57 5.57 0 0 1 7.808 0 5.408 5.408 0 0 1 0 7.714c-3.881 3.834-10.174 3.834-14.055 0a9.734 9.734 0 0 1-.015-13.87C5.096 1.35 8.138 0 11.5 0c3.75 0 7.105 1.68 9.333 4.319z" fill-rule="evenodd"/>
</svg>
</div>

<form
    class="bg-white shadow rounded-lg p-8 max-w-login mx-auto"
    method="POST"
    action="http://localhost/nova/password/email"
>
    <input type="hidden" name="_token" value="QDDXQdwZ7q0GV6J4jllefz6Q6xQsmlEfA3eT4Lf3">

    <h2 class="text-2xl text-center font-normal mb-6 text-90">Forgot your password?</h2>
<svg class="block mx-auto mb-6" xmlns="http://www.w3.org/2000/svg" width="100" height="2" viewBox="0 0 100 2">
  <path fill="#D8E3EC" d="M0 0h100v2H0z"/>
</svg>

    
    
    <div class="mb-6 ">
        <label class="block font-bold mb-2" for="email">Email Address</label>
        <input class="form-control form-input form-input-bordered w-full" id="email" type="email" name="email" value="" required>
    </div>

    <button class="w-full btn btn-default btn-primary hover:bg-primary-dark" type="submit">
        Send Password Reset Link
    </button>
</form>
        </div>
    </div>
</body>
</html>

```
<div id="execution-results-GETnova-password-reset" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-password-reset"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-password-reset"></code></pre>
</div>
<div id="execution-error-GETnova-password-reset" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-password-reset"></code></pre>
</div>
<form id="form-GETnova-password-reset" data-method="GET" data-path="nova/password/reset" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-password-reset', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-password-reset" onclick="tryItOut('GETnova-password-reset');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-password-reset" onclick="cancelTryOut('GETnova-password-reset');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-password-reset" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova/password/reset</code></b>
</p>
</form>


## Send a reset link to the given user.




> Example request:

```bash
curl -X POST \
    "http://localhost/nova/password/email" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova/password/email"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTnova-password-email" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTnova-password-email"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTnova-password-email"></code></pre>
</div>
<div id="execution-error-POSTnova-password-email" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTnova-password-email"></code></pre>
</div>
<form id="form-POSTnova-password-email" data-method="POST" data-path="nova/password/email" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTnova-password-email', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTnova-password-email" onclick="tryItOut('POSTnova-password-email');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTnova-password-email" onclick="cancelTryOut('POSTnova-password-email');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTnova-password-email" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>nova/password/email</code></b>
</p>
</form>


## Display the password reset view for the given token.


If no token is present, display the link request form.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova/password/reset/sunt" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova/password/reset/sunt"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json

<!DOCTYPE html>
<html lang="en" class="h-full font-sans antialiased">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="QDDXQdwZ7q0GV6J4jllefz6Q6xQsmlEfA3eT4Lf3">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,800,800i,900,900i" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="/vendor/nova/app.css?id=40717efbdd839ce5aad5">

    <!-- Custom Meta Data -->
    
    <!-- Theme Styles -->
    </head>
<body class="bg-40 text-black h-full">
    <div class="h-full">
        <div class="px-view py-view mx-auto">
            
<div class="mx-auto py-8 max-w-sm text-center text-90">
    <svg
    class="fill-current"
    width="200"
    height="39"
    viewBox="0 0 126 24"
    xmlns="http://www.w3.org/2000/svg"
>
    <path d="M40.76 18h-6.8V7.328h2.288V16h4.512v2zm8.064 0h-2.048v-.816c-.528.64-1.44 1.008-2.448 1.008-1.232 0-2.672-.832-2.672-2.56 0-1.824 1.44-2.496 2.672-2.496 1.04 0 1.936.336 2.448.944v-.976c0-.784-.672-1.296-1.696-1.296-.816 0-1.584.32-2.224.912l-.8-1.424c.944-.848 2.16-1.216 3.376-1.216 1.776 0 3.392.704 3.392 2.928V18zm-3.68-1.184c.656 0 1.296-.224 1.632-.672v-.96c-.336-.448-.976-.688-1.632-.688-.8 0-1.456.432-1.456 1.168s.656 1.152 1.456 1.152zM52.856 18h-2.032v-7.728h2.032v1.04c.56-.672 1.504-1.232 2.464-1.232v1.984a2.595 2.595 0 0 0-.56-.048c-.672 0-1.568.384-1.904.88V18zm10.416 0h-2.048v-.816c-.528.64-1.44 1.008-2.448 1.008-1.232 0-2.672-.832-2.672-2.56 0-1.824 1.44-2.496 2.672-2.496 1.04 0 1.936.336 2.448.944v-.976c0-.784-.672-1.296-1.696-1.296-.816 0-1.584.32-2.224.912l-.8-1.424c.944-.848 2.16-1.216 3.376-1.216 1.776 0 3.392.704 3.392 2.928V18zm-3.68-1.184c.656 0 1.296-.224 1.632-.672v-.96c-.336-.448-.976-.688-1.632-.688-.8 0-1.456.432-1.456 1.168s.656 1.152 1.456 1.152zM69.464 18h-2.192l-3.104-7.728h2.176l2.016 5.376 2.032-5.376h2.176L69.464 18zm7.648.192c-2.352 0-4.128-1.584-4.128-4.064 0-2.24 1.664-4.048 4-4.048 2.32 0 3.872 1.728 3.872 4.24v.48h-5.744c.144.944.912 1.728 2.224 1.728.656 0 1.552-.272 2.048-.752l.912 1.344c-.768.704-1.984 1.072-3.184 1.072zm1.792-4.8c-.064-.736-.576-1.648-1.92-1.648-1.264 0-1.808.88-1.888 1.648h3.808zM84.36 18h-2.032V7.328h2.032V18zm15.232 0h-1.28l-6.224-8.512V18H90.76V7.328h1.36l6.144 8.336V7.328h1.328V18zm5.824.192c-2.352 0-3.824-1.824-3.824-4.064s1.472-4.048 3.824-4.048 3.824 1.808 3.824 4.048-1.472 4.064-3.824 4.064zm0-1.072c1.648 0 2.56-1.408 2.56-2.992 0-1.568-.912-2.976-2.56-2.976-1.648 0-2.56 1.408-2.56 2.976 0 1.584.912 2.992 2.56 2.992zm9.152.88h-1.312l-3.216-7.728h1.312l2.56 6.336 2.576-6.336h1.296L114.568 18zm10.496 0h-1.2v-.88c-.624.704-1.52 1.072-2.56 1.072-1.296 0-2.688-.88-2.688-2.56 0-1.744 1.376-2.544 2.688-2.544 1.056 0 1.936.336 2.56 1.04v-1.392c0-1.024-.832-1.616-1.952-1.616-.928 0-1.68.32-2.368 1.072l-.56-.832c.832-.864 1.824-1.28 3.088-1.28 1.648 0 2.992.736 2.992 2.608V18zm-3.312-.672c.832 0 1.648-.32 2.112-.96v-1.472c-.464-.624-1.28-.944-2.112-.944-1.136 0-1.92.704-1.92 1.68 0 .992.784 1.696 1.92 1.696zM20.119 20.455A12.184 12.184 0 0 1 11.5 24a12.18 12.18 0 0 1-9.333-4.319c4.772 3.933 11.88 3.687 16.36-.738a7.571 7.571 0 0 0 0-10.8c-3.018-2.982-7.912-2.982-10.931 0a3.245 3.245 0 0 0 0 4.628 3.342 3.342 0 0 0 4.685 0 1.114 1.114 0 0 1 1.561 0 1.082 1.082 0 0 1 0 1.543 5.57 5.57 0 0 1-7.808 0 5.408 5.408 0 0 1 0-7.714c3.881-3.834 10.174-3.834 14.055 0a9.734 9.734 0 0 1 .03 13.855zm.714-16.136C16.06.386 8.953.632 4.473 5.057a7.571 7.571 0 0 0 0 10.8c3.018 2.982 7.912 2.982 10.931 0a3.245 3.245 0 0 0 0-4.628 3.342 3.342 0 0 0-4.685 0 1.114 1.114 0 0 1-1.561 0 1.082 1.082 0 0 1 0-1.543 5.57 5.57 0 0 1 7.808 0 5.408 5.408 0 0 1 0 7.714c-3.881 3.834-10.174 3.834-14.055 0a9.734 9.734 0 0 1-.015-13.87C5.096 1.35 8.138 0 11.5 0c3.75 0 7.105 1.68 9.333 4.319z" fill-rule="evenodd"/>
</svg>
</div>

<form
    class="bg-white shadow rounded-lg p-8 max-w-login mx-auto"
    method="POST"
    action="http://localhost/nova/password/reset"
>
    <input type="hidden" name="_token" value="QDDXQdwZ7q0GV6J4jllefz6Q6xQsmlEfA3eT4Lf3">

    <h2 class="text-2xl text-center font-normal mb-6 text-90">Reset Password</h2>
<svg class="block mx-auto mb-6" xmlns="http://www.w3.org/2000/svg" width="100" height="2" viewBox="0 0 100 2">
  <path fill="#D8E3EC" d="M0 0h100v2H0z"/>
</svg>

    
    <input type="hidden" name="token" value="sunt">

    <div class="mb-6 ">
        <label class="block font-bold mb-2" for="email">Email Address</label>
        <input class="form-control form-input form-input-bordered w-full" id="email" type="email" name="email" value="" required autofocus>
    </div>

    <div class="mb-6 ">
        <label class="block font-bold mb-2" for="password">Password</label>
        <input class="form-control form-input form-input-bordered w-full" id="password" type="password" name="password" required>
    </div>

    <div class="mb-6 ">
        <label class="block font-bold mb-2" for="password-confirm">Confirm Password</label>
        <input class="form-control form-input form-input-bordered w-full" id="password-confirm" type="password" name="password_confirmation" required>
    </div>

    <button class="w-full btn btn-default btn-primary hover:bg-primary-dark" type="submit">
        Reset Password
    </button>
</form>
        </div>
    </div>
</body>
</html>

```
<div id="execution-results-GETnova-password-reset--token-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-password-reset--token-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-password-reset--token-"></code></pre>
</div>
<div id="execution-error-GETnova-password-reset--token-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-password-reset--token-"></code></pre>
</div>
<form id="form-GETnova-password-reset--token-" data-method="GET" data-path="nova/password/reset/{token}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-password-reset--token-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-password-reset--token-" onclick="tryItOut('GETnova-password-reset--token-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-password-reset--token-" onclick="cancelTryOut('GETnova-password-reset--token-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-password-reset--token-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova/password/reset/{token}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="token" data-endpoint="GETnova-password-reset--token-" data-component="url" required  hidden>
<br>
</p>
</form>


## Reset the given user&#039;s password.




> Example request:

```bash
curl -X POST \
    "http://localhost/nova/password/reset" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova/password/reset"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTnova-password-reset" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTnova-password-reset"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTnova-password-reset"></code></pre>
</div>
<div id="execution-error-POSTnova-password-reset" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTnova-password-reset"></code></pre>
</div>
<form id="form-POSTnova-password-reset" data-method="POST" data-path="nova/password/reset" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTnova-password-reset', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTnova-password-reset" onclick="tryItOut('POSTnova-password-reset');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTnova-password-reset" onclick="cancelTryOut('POSTnova-password-reset');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTnova-password-reset" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>nova/password/reset</code></b>
</p>
</form>


## Serve the requested script.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/scripts/sunt" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/scripts/sunt"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api-scripts--script-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api-scripts--script-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api-scripts--script-"></code></pre>
</div>
<div id="execution-error-GETnova-api-scripts--script-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api-scripts--script-"></code></pre>
</div>
<form id="form-GETnova-api-scripts--script-" data-method="GET" data-path="nova-api/scripts/{script}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api-scripts--script-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api-scripts--script-" onclick="tryItOut('GETnova-api-scripts--script-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api-scripts--script-" onclick="cancelTryOut('GETnova-api-scripts--script-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api-scripts--script-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/scripts/{script}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>script</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="script" data-endpoint="GETnova-api-scripts--script-" data-component="url" required  hidden>
<br>
</p>
</form>


## Serve the requested stylesheet.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/styles/ut" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/styles/ut"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api-styles--style-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api-styles--style-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api-styles--style-"></code></pre>
</div>
<div id="execution-error-GETnova-api-styles--style-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api-styles--style-"></code></pre>
</div>
<form id="form-GETnova-api-styles--style-" data-method="GET" data-path="nova-api/styles/{style}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api-styles--style-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api-styles--style-" onclick="tryItOut('GETnova-api-styles--style-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api-styles--style-" onclick="cancelTryOut('GETnova-api-styles--style-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api-styles--style-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/styles/{style}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>style</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="style" data-endpoint="GETnova-api-styles--style-" data-component="url" required  hidden>
<br>
</p>
</form>


## Get the global search results for the given query.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/search" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/search"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api-search" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api-search"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api-search"></code></pre>
</div>
<div id="execution-error-GETnova-api-search" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api-search"></code></pre>
</div>
<form id="form-GETnova-api-search" data-method="GET" data-path="nova-api/search" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api-search', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api-search" onclick="tryItOut('GETnova-api-search');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api-search" onclick="cancelTryOut('GETnova-api-search');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api-search" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/search</code></b>
</p>
</form>


## Retrieve the given field for the given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/necessitatibus/field/ut" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/necessitatibus/field/ut"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--field--field-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--field--field-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--field--field-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--field--field-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--field--field-"></code></pre>
</div>
<form id="form-GETnova-api--resource--field--field-" data-method="GET" data-path="nova-api/{resource}/field/{field}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--field--field-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--field--field-" onclick="tryItOut('GETnova-api--resource--field--field-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--field--field-" onclick="cancelTryOut('GETnova-api--resource--field--field-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--field--field-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/field/{field}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--field--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>field</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="field" data-endpoint="GETnova-api--resource--field--field-" data-component="url" required  hidden>
<br>
</p>
</form>


## Store an attachment for a Trix field.




> Example request:

```bash
curl -X POST \
    "http://localhost/nova-api/quidem/trix-attachment/exercitationem" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/quidem/trix-attachment/exercitationem"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTnova-api--resource--trix-attachment--field-" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTnova-api--resource--trix-attachment--field-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTnova-api--resource--trix-attachment--field-"></code></pre>
</div>
<div id="execution-error-POSTnova-api--resource--trix-attachment--field-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTnova-api--resource--trix-attachment--field-"></code></pre>
</div>
<form id="form-POSTnova-api--resource--trix-attachment--field-" data-method="POST" data-path="nova-api/{resource}/trix-attachment/{field}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTnova-api--resource--trix-attachment--field-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTnova-api--resource--trix-attachment--field-" onclick="tryItOut('POSTnova-api--resource--trix-attachment--field-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTnova-api--resource--trix-attachment--field-" onclick="cancelTryOut('POSTnova-api--resource--trix-attachment--field-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTnova-api--resource--trix-attachment--field-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>nova-api/{resource}/trix-attachment/{field}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="POSTnova-api--resource--trix-attachment--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>field</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="field" data-endpoint="POSTnova-api--resource--trix-attachment--field-" data-component="url" required  hidden>
<br>
</p>
</form>


## Delete a single, persisted attachment for a Trix field by URL.




> Example request:

```bash
curl -X DELETE \
    "http://localhost/nova-api/adipisci/trix-attachment/et" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/adipisci/trix-attachment/et"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEnova-api--resource--trix-attachment--field-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEnova-api--resource--trix-attachment--field-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEnova-api--resource--trix-attachment--field-"></code></pre>
</div>
<div id="execution-error-DELETEnova-api--resource--trix-attachment--field-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEnova-api--resource--trix-attachment--field-"></code></pre>
</div>
<form id="form-DELETEnova-api--resource--trix-attachment--field-" data-method="DELETE" data-path="nova-api/{resource}/trix-attachment/{field}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEnova-api--resource--trix-attachment--field-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEnova-api--resource--trix-attachment--field-" onclick="tryItOut('DELETEnova-api--resource--trix-attachment--field-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEnova-api--resource--trix-attachment--field-" onclick="cancelTryOut('DELETEnova-api--resource--trix-attachment--field-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEnova-api--resource--trix-attachment--field-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>nova-api/{resource}/trix-attachment/{field}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="DELETEnova-api--resource--trix-attachment--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>field</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="field" data-endpoint="DELETEnova-api--resource--trix-attachment--field-" data-component="url" required  hidden>
<br>
</p>
</form>


## Purge all pending attachments for a Trix field.




> Example request:

```bash
curl -X DELETE \
    "http://localhost/nova-api/fuga/trix-attachment/suscipit/sapiente" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/fuga/trix-attachment/suscipit/sapiente"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEnova-api--resource--trix-attachment--field---draftId-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEnova-api--resource--trix-attachment--field---draftId-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEnova-api--resource--trix-attachment--field---draftId-"></code></pre>
</div>
<div id="execution-error-DELETEnova-api--resource--trix-attachment--field---draftId-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEnova-api--resource--trix-attachment--field---draftId-"></code></pre>
</div>
<form id="form-DELETEnova-api--resource--trix-attachment--field---draftId-" data-method="DELETE" data-path="nova-api/{resource}/trix-attachment/{field}/{draftId}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEnova-api--resource--trix-attachment--field---draftId-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEnova-api--resource--trix-attachment--field---draftId-" onclick="tryItOut('DELETEnova-api--resource--trix-attachment--field---draftId-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEnova-api--resource--trix-attachment--field---draftId-" onclick="cancelTryOut('DELETEnova-api--resource--trix-attachment--field---draftId-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEnova-api--resource--trix-attachment--field---draftId-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>nova-api/{resource}/trix-attachment/{field}/{draftId}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="DELETEnova-api--resource--trix-attachment--field---draftId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>field</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="field" data-endpoint="DELETEnova-api--resource--trix-attachment--field---draftId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>draftId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="draftId" data-endpoint="DELETEnova-api--resource--trix-attachment--field---draftId-" data-component="url" required  hidden>
<br>
</p>
</form>


## List the creation fields for the given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/magni/creation-fields" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/magni/creation-fields"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--creation-fields" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--creation-fields"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--creation-fields"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--creation-fields" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--creation-fields"></code></pre>
</div>
<form id="form-GETnova-api--resource--creation-fields" data-method="GET" data-path="nova-api/{resource}/creation-fields" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--creation-fields', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--creation-fields" onclick="tryItOut('GETnova-api--resource--creation-fields');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--creation-fields" onclick="cancelTryOut('GETnova-api--resource--creation-fields');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--creation-fields" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/creation-fields</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--creation-fields" data-component="url" required  hidden>
<br>
</p>
</form>


## List the update fields for the given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/ratione/tempore/update-fields" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/ratione/tempore/update-fields"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource---resourceId--update-fields" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource---resourceId--update-fields"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource---resourceId--update-fields"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource---resourceId--update-fields" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource---resourceId--update-fields"></code></pre>
</div>
<form id="form-GETnova-api--resource---resourceId--update-fields" data-method="GET" data-path="nova-api/{resource}/{resourceId}/update-fields" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource---resourceId--update-fields', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource---resourceId--update-fields" onclick="tryItOut('GETnova-api--resource---resourceId--update-fields');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource---resourceId--update-fields" onclick="cancelTryOut('GETnova-api--resource---resourceId--update-fields');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource---resourceId--update-fields" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/{resourceId}/update-fields</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource---resourceId--update-fields" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="GETnova-api--resource---resourceId--update-fields" data-component="url" required  hidden>
<br>
</p>
</form>


## List the pivot fields for the given resource and relation.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/neque/ipsa/creation-pivot-fields/occaecati" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/neque/ipsa/creation-pivot-fields/occaecati"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-"></code></pre>
</div>
<form id="form-GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-" data-method="GET" data-path="nova-api/{resource}/{resourceId}/creation-pivot-fields/{relatedResource}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-" onclick="tryItOut('GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-" onclick="cancelTryOut('GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/{resourceId}/creation-pivot-fields/{relatedResource}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>relatedResource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="relatedResource" data-endpoint="GETnova-api--resource---resourceId--creation-pivot-fields--relatedResource-" data-component="url" required  hidden>
<br>
</p>
</form>


## List the pivot fields for the given resource and relation.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/harum/quia/update-pivot-fields/voluptatem/qui" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/harum/quia/update-pivot-fields/voluptatem/qui"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-"></code></pre>
</div>
<form id="form-GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-" data-method="GET" data-path="nova-api/{resource}/{resourceId}/update-pivot-fields/{relatedResource}/{relatedResourceId}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-" onclick="tryItOut('GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-" onclick="cancelTryOut('GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/{resourceId}/update-pivot-fields/{relatedResource}/{relatedResourceId}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>relatedResource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="relatedResource" data-endpoint="GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>relatedResourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="relatedResourceId" data-endpoint="GETnova-api--resource---resourceId--update-pivot-fields--relatedResource---relatedResourceId-" data-component="url" required  hidden>
<br>
</p>
</form>


## Download the given field&#039;s contents.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/maxime/dolore/download/reiciendis" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/maxime/dolore/download/reiciendis"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource---resourceId--download--field-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource---resourceId--download--field-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource---resourceId--download--field-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource---resourceId--download--field-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource---resourceId--download--field-"></code></pre>
</div>
<form id="form-GETnova-api--resource---resourceId--download--field-" data-method="GET" data-path="nova-api/{resource}/{resourceId}/download/{field}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource---resourceId--download--field-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource---resourceId--download--field-" onclick="tryItOut('GETnova-api--resource---resourceId--download--field-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource---resourceId--download--field-" onclick="cancelTryOut('GETnova-api--resource---resourceId--download--field-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource---resourceId--download--field-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/{resourceId}/download/{field}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource---resourceId--download--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="GETnova-api--resource---resourceId--download--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>field</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="field" data-endpoint="GETnova-api--resource---resourceId--download--field-" data-component="url" required  hidden>
<br>
</p>
</form>


## Delete the file at the given field.




> Example request:

```bash
curl -X DELETE \
    "http://localhost/nova-api/sed/minima/field/voluptatum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/sed/minima/field/voluptatum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEnova-api--resource---resourceId--field--field-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEnova-api--resource---resourceId--field--field-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEnova-api--resource---resourceId--field--field-"></code></pre>
</div>
<div id="execution-error-DELETEnova-api--resource---resourceId--field--field-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEnova-api--resource---resourceId--field--field-"></code></pre>
</div>
<form id="form-DELETEnova-api--resource---resourceId--field--field-" data-method="DELETE" data-path="nova-api/{resource}/{resourceId}/field/{field}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEnova-api--resource---resourceId--field--field-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEnova-api--resource---resourceId--field--field-" onclick="tryItOut('DELETEnova-api--resource---resourceId--field--field-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEnova-api--resource---resourceId--field--field-" onclick="cancelTryOut('DELETEnova-api--resource---resourceId--field--field-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEnova-api--resource---resourceId--field--field-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>nova-api/{resource}/{resourceId}/field/{field}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="DELETEnova-api--resource---resourceId--field--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="DELETEnova-api--resource---resourceId--field--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>field</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="field" data-endpoint="DELETEnova-api--resource---resourceId--field--field-" data-component="url" required  hidden>
<br>
</p>
</form>


## Delete the file at the given field.




> Example request:

```bash
curl -X DELETE \
    "http://localhost/nova-api/sint/aperiam/similique/aut/field/magnam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/sint/aperiam/similique/aut/field/magnam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-"></code></pre>
</div>
<div id="execution-error-DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-"></code></pre>
</div>
<form id="form-DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-" data-method="DELETE" data-path="nova-api/{resource}/{resourceId}/{relatedResource}/{relatedResourceId}/field/{field}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-" onclick="tryItOut('DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-" onclick="cancelTryOut('DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>nova-api/{resource}/{resourceId}/{relatedResource}/{relatedResourceId}/field/{field}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>relatedResource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="relatedResource" data-endpoint="DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>relatedResourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="relatedResourceId" data-endpoint="DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>field</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="field" data-endpoint="DELETEnova-api--resource---resourceId---relatedResource---relatedResourceId--field--field-" data-component="url" required  hidden>
<br>
</p>
</form>


## Return the details for the Dashboard.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/dashboards/laborum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/dashboards/laborum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api-dashboards--dashboard-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api-dashboards--dashboard-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api-dashboards--dashboard-"></code></pre>
</div>
<div id="execution-error-GETnova-api-dashboards--dashboard-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api-dashboards--dashboard-"></code></pre>
</div>
<form id="form-GETnova-api-dashboards--dashboard-" data-method="GET" data-path="nova-api/dashboards/{dashboard}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api-dashboards--dashboard-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api-dashboards--dashboard-" onclick="tryItOut('GETnova-api-dashboards--dashboard-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api-dashboards--dashboard-" onclick="cancelTryOut('GETnova-api-dashboards--dashboard-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api-dashboards--dashboard-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/dashboards/{dashboard}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>dashboard</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="dashboard" data-endpoint="GETnova-api-dashboards--dashboard-" data-component="url" required  hidden>
<br>
</p>
</form>


## List the cards for the dashboard.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/dashboards/cards/aut" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/dashboards/cards/aut"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api-dashboards-cards--dashboard-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api-dashboards-cards--dashboard-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api-dashboards-cards--dashboard-"></code></pre>
</div>
<div id="execution-error-GETnova-api-dashboards-cards--dashboard-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api-dashboards-cards--dashboard-"></code></pre>
</div>
<form id="form-GETnova-api-dashboards-cards--dashboard-" data-method="GET" data-path="nova-api/dashboards/cards/{dashboard}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api-dashboards-cards--dashboard-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api-dashboards-cards--dashboard-" onclick="tryItOut('GETnova-api-dashboards-cards--dashboard-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api-dashboards-cards--dashboard-" onclick="cancelTryOut('GETnova-api-dashboards-cards--dashboard-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api-dashboards-cards--dashboard-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/dashboards/cards/{dashboard}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>dashboard</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="dashboard" data-endpoint="GETnova-api-dashboards-cards--dashboard-" data-component="url" required  hidden>
<br>
</p>
</form>


## List the actions for the given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/possimus/actions" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/possimus/actions"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--actions" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--actions"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--actions"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--actions" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--actions"></code></pre>
</div>
<form id="form-GETnova-api--resource--actions" data-method="GET" data-path="nova-api/{resource}/actions" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--actions', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--actions" onclick="tryItOut('GETnova-api--resource--actions');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--actions" onclick="cancelTryOut('GETnova-api--resource--actions');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--actions" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/actions</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--actions" data-component="url" required  hidden>
<br>
</p>
</form>


## Perform an action on the specified resources.




> Example request:

```bash
curl -X POST \
    "http://localhost/nova-api/quae/action" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/quae/action"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTnova-api--resource--action" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTnova-api--resource--action"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTnova-api--resource--action"></code></pre>
</div>
<div id="execution-error-POSTnova-api--resource--action" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTnova-api--resource--action"></code></pre>
</div>
<form id="form-POSTnova-api--resource--action" data-method="POST" data-path="nova-api/{resource}/action" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTnova-api--resource--action', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTnova-api--resource--action" onclick="tryItOut('POSTnova-api--resource--action');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTnova-api--resource--action" onclick="cancelTryOut('POSTnova-api--resource--action');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTnova-api--resource--action" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>nova-api/{resource}/action</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="POSTnova-api--resource--action" data-component="url" required  hidden>
<br>
</p>
</form>


## List the filters for the given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/quod/filters" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/quod/filters"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--filters" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--filters"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--filters"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--filters" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--filters"></code></pre>
</div>
<form id="form-GETnova-api--resource--filters" data-method="GET" data-path="nova-api/{resource}/filters" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--filters', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--filters" onclick="tryItOut('GETnova-api--resource--filters');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--filters" onclick="cancelTryOut('GETnova-api--resource--filters');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--filters" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/filters</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--filters" data-component="url" required  hidden>
<br>
</p>
</form>


## List the lenses for the given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/eligendi/lenses" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/eligendi/lenses"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--lenses" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--lenses"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--lenses"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--lenses" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--lenses"></code></pre>
</div>
<form id="form-GETnova-api--resource--lenses" data-method="GET" data-path="nova-api/{resource}/lenses" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--lenses', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--lenses" onclick="tryItOut('GETnova-api--resource--lenses');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--lenses" onclick="cancelTryOut('GETnova-api--resource--lenses');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--lenses" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/lenses</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--lenses" data-component="url" required  hidden>
<br>
</p>
</form>


## Get the specified lens and its resources.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/earum/lens/soluta" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/earum/lens/soluta"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--lens--lens-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--lens--lens-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--lens--lens-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--lens--lens-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--lens--lens-"></code></pre>
</div>
<form id="form-GETnova-api--resource--lens--lens-" data-method="GET" data-path="nova-api/{resource}/lens/{lens}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--lens--lens-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--lens--lens-" onclick="tryItOut('GETnova-api--resource--lens--lens-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--lens--lens-" onclick="cancelTryOut('GETnova-api--resource--lens--lens-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--lens--lens-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/lens/{lens}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--lens--lens-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>lens</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lens" data-endpoint="GETnova-api--resource--lens--lens-" data-component="url" required  hidden>
<br>
</p>
</form>


## Get the resource count for a given query.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/quibusdam/lens/explicabo/count" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/quibusdam/lens/explicabo/count"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--lens--lens--count" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--lens--lens--count"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--lens--lens--count"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--lens--lens--count" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--lens--lens--count"></code></pre>
</div>
<form id="form-GETnova-api--resource--lens--lens--count" data-method="GET" data-path="nova-api/{resource}/lens/{lens}/count" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--lens--lens--count', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--lens--lens--count" onclick="tryItOut('GETnova-api--resource--lens--lens--count');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--lens--lens--count" onclick="cancelTryOut('GETnova-api--resource--lens--lens--count');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--lens--lens--count" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/lens/{lens}/count</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--lens--lens--count" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>lens</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lens" data-endpoint="GETnova-api--resource--lens--lens--count" data-component="url" required  hidden>
<br>
</p>
</form>


## Destroy the given resource(s).




> Example request:

```bash
curl -X DELETE \
    "http://localhost/nova-api/suscipit/lens/fugiat" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/suscipit/lens/fugiat"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEnova-api--resource--lens--lens-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEnova-api--resource--lens--lens-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEnova-api--resource--lens--lens-"></code></pre>
</div>
<div id="execution-error-DELETEnova-api--resource--lens--lens-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEnova-api--resource--lens--lens-"></code></pre>
</div>
<form id="form-DELETEnova-api--resource--lens--lens-" data-method="DELETE" data-path="nova-api/{resource}/lens/{lens}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEnova-api--resource--lens--lens-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEnova-api--resource--lens--lens-" onclick="tryItOut('DELETEnova-api--resource--lens--lens-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEnova-api--resource--lens--lens-" onclick="cancelTryOut('DELETEnova-api--resource--lens--lens-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEnova-api--resource--lens--lens-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>nova-api/{resource}/lens/{lens}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="DELETEnova-api--resource--lens--lens-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>lens</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lens" data-endpoint="DELETEnova-api--resource--lens--lens-" data-component="url" required  hidden>
<br>
</p>
</form>


## Force delete the given resource(s).




> Example request:

```bash
curl -X DELETE \
    "http://localhost/nova-api/dolorem/lens/minima/force" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/dolorem/lens/minima/force"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEnova-api--resource--lens--lens--force" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEnova-api--resource--lens--lens--force"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEnova-api--resource--lens--lens--force"></code></pre>
</div>
<div id="execution-error-DELETEnova-api--resource--lens--lens--force" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEnova-api--resource--lens--lens--force"></code></pre>
</div>
<form id="form-DELETEnova-api--resource--lens--lens--force" data-method="DELETE" data-path="nova-api/{resource}/lens/{lens}/force" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEnova-api--resource--lens--lens--force', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEnova-api--resource--lens--lens--force" onclick="tryItOut('DELETEnova-api--resource--lens--lens--force');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEnova-api--resource--lens--lens--force" onclick="cancelTryOut('DELETEnova-api--resource--lens--lens--force');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEnova-api--resource--lens--lens--force" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>nova-api/{resource}/lens/{lens}/force</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="DELETEnova-api--resource--lens--lens--force" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>lens</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lens" data-endpoint="DELETEnova-api--resource--lens--lens--force" data-component="url" required  hidden>
<br>
</p>
</form>


## Force delete the given resource(s).




> Example request:

```bash
curl -X PUT \
    "http://localhost/nova-api/cupiditate/lens/dolore/restore" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/cupiditate/lens/dolore/restore"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PUT",
    headers,
}).then(response => response.json());
```


<div id="execution-results-PUTnova-api--resource--lens--lens--restore" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTnova-api--resource--lens--lens--restore"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTnova-api--resource--lens--lens--restore"></code></pre>
</div>
<div id="execution-error-PUTnova-api--resource--lens--lens--restore" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTnova-api--resource--lens--lens--restore"></code></pre>
</div>
<form id="form-PUTnova-api--resource--lens--lens--restore" data-method="PUT" data-path="nova-api/{resource}/lens/{lens}/restore" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTnova-api--resource--lens--lens--restore', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTnova-api--resource--lens--lens--restore" onclick="tryItOut('PUTnova-api--resource--lens--lens--restore');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTnova-api--resource--lens--lens--restore" onclick="cancelTryOut('PUTnova-api--resource--lens--lens--restore');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTnova-api--resource--lens--lens--restore" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>nova-api/{resource}/lens/{lens}/restore</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="PUTnova-api--resource--lens--lens--restore" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>lens</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lens" data-endpoint="PUTnova-api--resource--lens--lens--restore" data-component="url" required  hidden>
<br>
</p>
</form>


## List the actions for the given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/pariatur/lens/et/actions" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/pariatur/lens/et/actions"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--lens--lens--actions" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--lens--lens--actions"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--lens--lens--actions"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--lens--lens--actions" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--lens--lens--actions"></code></pre>
</div>
<form id="form-GETnova-api--resource--lens--lens--actions" data-method="GET" data-path="nova-api/{resource}/lens/{lens}/actions" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--lens--lens--actions', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--lens--lens--actions" onclick="tryItOut('GETnova-api--resource--lens--lens--actions');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--lens--lens--actions" onclick="cancelTryOut('GETnova-api--resource--lens--lens--actions');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--lens--lens--actions" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/lens/{lens}/actions</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--lens--lens--actions" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>lens</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lens" data-endpoint="GETnova-api--resource--lens--lens--actions" data-component="url" required  hidden>
<br>
</p>
</form>


## Perform an action on the specified resources.




> Example request:

```bash
curl -X POST \
    "http://localhost/nova-api/eum/lens/modi/action" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/eum/lens/modi/action"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTnova-api--resource--lens--lens--action" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTnova-api--resource--lens--lens--action"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTnova-api--resource--lens--lens--action"></code></pre>
</div>
<div id="execution-error-POSTnova-api--resource--lens--lens--action" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTnova-api--resource--lens--lens--action"></code></pre>
</div>
<form id="form-POSTnova-api--resource--lens--lens--action" data-method="POST" data-path="nova-api/{resource}/lens/{lens}/action" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTnova-api--resource--lens--lens--action', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTnova-api--resource--lens--lens--action" onclick="tryItOut('POSTnova-api--resource--lens--lens--action');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTnova-api--resource--lens--lens--action" onclick="cancelTryOut('POSTnova-api--resource--lens--lens--action');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTnova-api--resource--lens--lens--action" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>nova-api/{resource}/lens/{lens}/action</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="POSTnova-api--resource--lens--lens--action" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>lens</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lens" data-endpoint="POSTnova-api--resource--lens--lens--action" data-component="url" required  hidden>
<br>
</p>
</form>


## List the lenses for the given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/nobis/lens/autem/filters" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/nobis/lens/autem/filters"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--lens--lens--filters" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--lens--lens--filters"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--lens--lens--filters"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--lens--lens--filters" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--lens--lens--filters"></code></pre>
</div>
<form id="form-GETnova-api--resource--lens--lens--filters" data-method="GET" data-path="nova-api/{resource}/lens/{lens}/filters" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--lens--lens--filters', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--lens--lens--filters" onclick="tryItOut('GETnova-api--resource--lens--lens--filters');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--lens--lens--filters" onclick="cancelTryOut('GETnova-api--resource--lens--lens--filters');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--lens--lens--filters" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/lens/{lens}/filters</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--lens--lens--filters" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>lens</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lens" data-endpoint="GETnova-api--resource--lens--lens--filters" data-component="url" required  hidden>
<br>
</p>
</form>


## List the metrics for the dashboard.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/metrics" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/metrics"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api-metrics" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api-metrics"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api-metrics"></code></pre>
</div>
<div id="execution-error-GETnova-api-metrics" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api-metrics"></code></pre>
</div>
<form id="form-GETnova-api-metrics" data-method="GET" data-path="nova-api/metrics" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api-metrics', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api-metrics" onclick="tryItOut('GETnova-api-metrics');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api-metrics" onclick="cancelTryOut('GETnova-api-metrics');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api-metrics" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/metrics</code></b>
</p>
</form>


## Get the specified metric&#039;s value.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/metrics/aperiam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/metrics/aperiam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api-metrics--metric-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api-metrics--metric-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api-metrics--metric-"></code></pre>
</div>
<div id="execution-error-GETnova-api-metrics--metric-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api-metrics--metric-"></code></pre>
</div>
<form id="form-GETnova-api-metrics--metric-" data-method="GET" data-path="nova-api/metrics/{metric}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api-metrics--metric-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api-metrics--metric-" onclick="tryItOut('GETnova-api-metrics--metric-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api-metrics--metric-" onclick="cancelTryOut('GETnova-api-metrics--metric-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api-metrics--metric-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/metrics/{metric}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>metric</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="metric" data-endpoint="GETnova-api-metrics--metric-" data-component="url" required  hidden>
<br>
</p>
</form>


## List the metrics for the given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/quaerat/metrics" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/quaerat/metrics"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--metrics" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--metrics"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--metrics"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--metrics" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--metrics"></code></pre>
</div>
<form id="form-GETnova-api--resource--metrics" data-method="GET" data-path="nova-api/{resource}/metrics" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--metrics', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--metrics" onclick="tryItOut('GETnova-api--resource--metrics');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--metrics" onclick="cancelTryOut('GETnova-api--resource--metrics');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--metrics" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/metrics</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--metrics" data-component="url" required  hidden>
<br>
</p>
</form>


## Get the specified metric&#039;s value.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/accusamus/metrics/iste" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/accusamus/metrics/iste"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--metrics--metric-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--metrics--metric-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--metrics--metric-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--metrics--metric-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--metrics--metric-"></code></pre>
</div>
<form id="form-GETnova-api--resource--metrics--metric-" data-method="GET" data-path="nova-api/{resource}/metrics/{metric}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--metrics--metric-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--metrics--metric-" onclick="tryItOut('GETnova-api--resource--metrics--metric-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--metrics--metric-" onclick="cancelTryOut('GETnova-api--resource--metrics--metric-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--metrics--metric-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/metrics/{metric}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--metrics--metric-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>metric</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="metric" data-endpoint="GETnova-api--resource--metrics--metric-" data-component="url" required  hidden>
<br>
</p>
</form>


## Get the specified metric&#039;s value.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/officia/quidem/metrics/in" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/officia/quidem/metrics/in"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource---resourceId--metrics--metric-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource---resourceId--metrics--metric-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource---resourceId--metrics--metric-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource---resourceId--metrics--metric-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource---resourceId--metrics--metric-"></code></pre>
</div>
<form id="form-GETnova-api--resource---resourceId--metrics--metric-" data-method="GET" data-path="nova-api/{resource}/{resourceId}/metrics/{metric}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource---resourceId--metrics--metric-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource---resourceId--metrics--metric-" onclick="tryItOut('GETnova-api--resource---resourceId--metrics--metric-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource---resourceId--metrics--metric-" onclick="cancelTryOut('GETnova-api--resource---resourceId--metrics--metric-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource---resourceId--metrics--metric-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/{resourceId}/metrics/{metric}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource---resourceId--metrics--metric-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="GETnova-api--resource---resourceId--metrics--metric-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>metric</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="metric" data-endpoint="GETnova-api--resource---resourceId--metrics--metric-" data-component="url" required  hidden>
<br>
</p>
</form>


## List the metrics for the given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/qui/lens/eos/metrics" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/qui/lens/eos/metrics"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--lens--lens--metrics" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--lens--lens--metrics"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--lens--lens--metrics"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--lens--lens--metrics" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--lens--lens--metrics"></code></pre>
</div>
<form id="form-GETnova-api--resource--lens--lens--metrics" data-method="GET" data-path="nova-api/{resource}/lens/{lens}/metrics" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--lens--lens--metrics', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--lens--lens--metrics" onclick="tryItOut('GETnova-api--resource--lens--lens--metrics');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--lens--lens--metrics" onclick="cancelTryOut('GETnova-api--resource--lens--lens--metrics');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--lens--lens--metrics" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/lens/{lens}/metrics</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--lens--lens--metrics" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>lens</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lens" data-endpoint="GETnova-api--resource--lens--lens--metrics" data-component="url" required  hidden>
<br>
</p>
</form>


## Get the specified metric&#039;s value.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/hic/lens/officia/metrics/molestiae" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/hic/lens/officia/metrics/molestiae"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--lens--lens--metrics--metric-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--lens--lens--metrics--metric-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--lens--lens--metrics--metric-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--lens--lens--metrics--metric-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--lens--lens--metrics--metric-"></code></pre>
</div>
<form id="form-GETnova-api--resource--lens--lens--metrics--metric-" data-method="GET" data-path="nova-api/{resource}/lens/{lens}/metrics/{metric}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--lens--lens--metrics--metric-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--lens--lens--metrics--metric-" onclick="tryItOut('GETnova-api--resource--lens--lens--metrics--metric-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--lens--lens--metrics--metric-" onclick="cancelTryOut('GETnova-api--resource--lens--lens--metrics--metric-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--lens--lens--metrics--metric-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/lens/{lens}/metrics/{metric}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--lens--lens--metrics--metric-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>lens</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lens" data-endpoint="GETnova-api--resource--lens--lens--metrics--metric-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>metric</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="metric" data-endpoint="GETnova-api--resource--lens--lens--metrics--metric-" data-component="url" required  hidden>
<br>
</p>
</form>


## List the cards for the dashboard.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/cards" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/cards"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api-cards" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api-cards"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api-cards"></code></pre>
</div>
<div id="execution-error-GETnova-api-cards" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api-cards"></code></pre>
</div>
<form id="form-GETnova-api-cards" data-method="GET" data-path="nova-api/cards" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api-cards', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api-cards" onclick="tryItOut('GETnova-api-cards');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api-cards" onclick="cancelTryOut('GETnova-api-cards');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api-cards" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/cards</code></b>
</p>
</form>


## List the cards for the given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/aperiam/cards" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/aperiam/cards"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--cards" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--cards"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--cards"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--cards" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--cards"></code></pre>
</div>
<form id="form-GETnova-api--resource--cards" data-method="GET" data-path="nova-api/{resource}/cards" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--cards', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--cards" onclick="tryItOut('GETnova-api--resource--cards');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--cards" onclick="cancelTryOut('GETnova-api--resource--cards');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--cards" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/cards</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--cards" data-component="url" required  hidden>
<br>
</p>
</form>


## List the cards for the given lens.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/quia/lens/recusandae/cards" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/quia/lens/recusandae/cards"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--lens--lens--cards" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--lens--lens--cards"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--lens--lens--cards"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--lens--lens--cards" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--lens--lens--cards"></code></pre>
</div>
<form id="form-GETnova-api--resource--lens--lens--cards" data-method="GET" data-path="nova-api/{resource}/lens/{lens}/cards" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--lens--lens--cards', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--lens--lens--cards" onclick="tryItOut('GETnova-api--resource--lens--lens--cards');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--lens--lens--cards" onclick="cancelTryOut('GETnova-api--resource--lens--lens--cards');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--lens--lens--cards" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/lens/{lens}/cards</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--lens--lens--cards" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>lens</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="lens" data-endpoint="GETnova-api--resource--lens--lens--cards" data-component="url" required  hidden>
<br>
</p>
</form>


## Get the relatable authorization status for the resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/placeat/relate-authorization" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/placeat/relate-authorization"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--relate-authorization" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--relate-authorization"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--relate-authorization"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--relate-authorization" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--relate-authorization"></code></pre>
</div>
<form id="form-GETnova-api--resource--relate-authorization" data-method="GET" data-path="nova-api/{resource}/relate-authorization" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--relate-authorization', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--relate-authorization" onclick="tryItOut('GETnova-api--resource--relate-authorization');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--relate-authorization" onclick="cancelTryOut('GETnova-api--resource--relate-authorization');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--relate-authorization" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/relate-authorization</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--relate-authorization" data-component="url" required  hidden>
<br>
</p>
</form>


## Determine if the resource is soft deleting.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/quis/soft-deletes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/quis/soft-deletes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--soft-deletes" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--soft-deletes"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--soft-deletes"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--soft-deletes" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--soft-deletes"></code></pre>
</div>
<form id="form-GETnova-api--resource--soft-deletes" data-method="GET" data-path="nova-api/{resource}/soft-deletes" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--soft-deletes', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--soft-deletes" onclick="tryItOut('GETnova-api--resource--soft-deletes');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--soft-deletes" onclick="cancelTryOut('GETnova-api--resource--soft-deletes');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--soft-deletes" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/soft-deletes</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--soft-deletes" data-component="url" required  hidden>
<br>
</p>
</form>


## List the resources for administration.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/laudantium" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/laudantium"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource-"></code></pre>
</div>
<form id="form-GETnova-api--resource-" data-method="GET" data-path="nova-api/{resource}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource-" onclick="tryItOut('GETnova-api--resource-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource-" onclick="cancelTryOut('GETnova-api--resource-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource-" data-component="url" required  hidden>
<br>
</p>
</form>


## Get the resource count for a given query.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/voluptatem/count" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/voluptatem/count"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--count" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--count"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--count"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--count" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--count"></code></pre>
</div>
<form id="form-GETnova-api--resource--count" data-method="GET" data-path="nova-api/{resource}/count" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--count', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--count" onclick="tryItOut('GETnova-api--resource--count');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--count" onclick="cancelTryOut('GETnova-api--resource--count');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--count" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/count</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--count" data-component="url" required  hidden>
<br>
</p>
</form>


## Detach the given resource(s).




> Example request:

```bash
curl -X DELETE \
    "http://localhost/nova-api/aliquid/detach" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/aliquid/detach"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEnova-api--resource--detach" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEnova-api--resource--detach"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEnova-api--resource--detach"></code></pre>
</div>
<div id="execution-error-DELETEnova-api--resource--detach" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEnova-api--resource--detach"></code></pre>
</div>
<form id="form-DELETEnova-api--resource--detach" data-method="DELETE" data-path="nova-api/{resource}/detach" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEnova-api--resource--detach', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEnova-api--resource--detach" onclick="tryItOut('DELETEnova-api--resource--detach');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEnova-api--resource--detach" onclick="cancelTryOut('DELETEnova-api--resource--detach');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEnova-api--resource--detach" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>nova-api/{resource}/detach</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="DELETEnova-api--resource--detach" data-component="url" required  hidden>
<br>
</p>
</form>


## Restore the given resource(s).




> Example request:

```bash
curl -X PUT \
    "http://localhost/nova-api/illo/restore" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/illo/restore"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PUT",
    headers,
}).then(response => response.json());
```


<div id="execution-results-PUTnova-api--resource--restore" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTnova-api--resource--restore"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTnova-api--resource--restore"></code></pre>
</div>
<div id="execution-error-PUTnova-api--resource--restore" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTnova-api--resource--restore"></code></pre>
</div>
<form id="form-PUTnova-api--resource--restore" data-method="PUT" data-path="nova-api/{resource}/restore" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTnova-api--resource--restore', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTnova-api--resource--restore" onclick="tryItOut('PUTnova-api--resource--restore');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTnova-api--resource--restore" onclick="cancelTryOut('PUTnova-api--resource--restore');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTnova-api--resource--restore" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>nova-api/{resource}/restore</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="PUTnova-api--resource--restore" data-component="url" required  hidden>
<br>
</p>
</form>


## Force delete the given resource(s).




> Example request:

```bash
curl -X DELETE \
    "http://localhost/nova-api/aut/force" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/aut/force"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEnova-api--resource--force" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEnova-api--resource--force"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEnova-api--resource--force"></code></pre>
</div>
<div id="execution-error-DELETEnova-api--resource--force" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEnova-api--resource--force"></code></pre>
</div>
<form id="form-DELETEnova-api--resource--force" data-method="DELETE" data-path="nova-api/{resource}/force" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEnova-api--resource--force', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEnova-api--resource--force" onclick="tryItOut('DELETEnova-api--resource--force');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEnova-api--resource--force" onclick="cancelTryOut('DELETEnova-api--resource--force');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEnova-api--resource--force" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>nova-api/{resource}/force</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="DELETEnova-api--resource--force" data-component="url" required  hidden>
<br>
</p>
</form>


## Display the resource for administration.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/eos/culpa" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/eos/culpa"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource---resourceId-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource---resourceId-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource---resourceId-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource---resourceId-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource---resourceId-"></code></pre>
</div>
<form id="form-GETnova-api--resource---resourceId-" data-method="GET" data-path="nova-api/{resource}/{resourceId}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource---resourceId-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource---resourceId-" onclick="tryItOut('GETnova-api--resource---resourceId-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource---resourceId-" onclick="cancelTryOut('GETnova-api--resource---resourceId-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource---resourceId-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/{resourceId}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource---resourceId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="GETnova-api--resource---resourceId-" data-component="url" required  hidden>
<br>
</p>
</form>


## Create a new resource.




> Example request:

```bash
curl -X POST \
    "http://localhost/nova-api/unde" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/unde"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTnova-api--resource-" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTnova-api--resource-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTnova-api--resource-"></code></pre>
</div>
<div id="execution-error-POSTnova-api--resource-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTnova-api--resource-"></code></pre>
</div>
<form id="form-POSTnova-api--resource-" data-method="POST" data-path="nova-api/{resource}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTnova-api--resource-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTnova-api--resource-" onclick="tryItOut('POSTnova-api--resource-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTnova-api--resource-" onclick="cancelTryOut('POSTnova-api--resource-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTnova-api--resource-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>nova-api/{resource}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="POSTnova-api--resource-" data-component="url" required  hidden>
<br>
</p>
</form>


## Create a new resource.




> Example request:

```bash
curl -X PUT \
    "http://localhost/nova-api/illo/dolores" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/illo/dolores"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PUT",
    headers,
}).then(response => response.json());
```


<div id="execution-results-PUTnova-api--resource---resourceId-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTnova-api--resource---resourceId-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTnova-api--resource---resourceId-"></code></pre>
</div>
<div id="execution-error-PUTnova-api--resource---resourceId-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTnova-api--resource---resourceId-"></code></pre>
</div>
<form id="form-PUTnova-api--resource---resourceId-" data-method="PUT" data-path="nova-api/{resource}/{resourceId}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTnova-api--resource---resourceId-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTnova-api--resource---resourceId-" onclick="tryItOut('PUTnova-api--resource---resourceId-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTnova-api--resource---resourceId-" onclick="cancelTryOut('PUTnova-api--resource---resourceId-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTnova-api--resource---resourceId-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>nova-api/{resource}/{resourceId}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="PUTnova-api--resource---resourceId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="PUTnova-api--resource---resourceId-" data-component="url" required  hidden>
<br>
</p>
</form>


## Destroy the given resource(s).




> Example request:

```bash
curl -X DELETE \
    "http://localhost/nova-api/in" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/in"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEnova-api--resource-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEnova-api--resource-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEnova-api--resource-"></code></pre>
</div>
<div id="execution-error-DELETEnova-api--resource-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEnova-api--resource-"></code></pre>
</div>
<form id="form-DELETEnova-api--resource-" data-method="DELETE" data-path="nova-api/{resource}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEnova-api--resource-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEnova-api--resource-" onclick="tryItOut('DELETEnova-api--resource-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEnova-api--resource-" onclick="cancelTryOut('DELETEnova-api--resource-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEnova-api--resource-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>nova-api/{resource}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="DELETEnova-api--resource-" data-component="url" required  hidden>
<br>
</p>
</form>


## List the available related resources for a given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/optio/associatable/rerum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/optio/associatable/rerum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--associatable--field-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--associatable--field-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--associatable--field-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--associatable--field-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--associatable--field-"></code></pre>
</div>
<form id="form-GETnova-api--resource--associatable--field-" data-method="GET" data-path="nova-api/{resource}/associatable/{field}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--associatable--field-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--associatable--field-" onclick="tryItOut('GETnova-api--resource--associatable--field-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--associatable--field-" onclick="cancelTryOut('GETnova-api--resource--associatable--field-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--associatable--field-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/associatable/{field}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--associatable--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>field</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="field" data-endpoint="GETnova-api--resource--associatable--field-" data-component="url" required  hidden>
<br>
</p>
</form>


## List the available related resources for a given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/sit/et/attachable/magni" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/sit/et/attachable/magni"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource---resourceId--attachable--field-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource---resourceId--attachable--field-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource---resourceId--attachable--field-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource---resourceId--attachable--field-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource---resourceId--attachable--field-"></code></pre>
</div>
<form id="form-GETnova-api--resource---resourceId--attachable--field-" data-method="GET" data-path="nova-api/{resource}/{resourceId}/attachable/{field}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource---resourceId--attachable--field-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource---resourceId--attachable--field-" onclick="tryItOut('GETnova-api--resource---resourceId--attachable--field-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource---resourceId--attachable--field-" onclick="cancelTryOut('GETnova-api--resource---resourceId--attachable--field-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource---resourceId--attachable--field-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/{resourceId}/attachable/{field}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource---resourceId--attachable--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="GETnova-api--resource---resourceId--attachable--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>field</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="field" data-endpoint="GETnova-api--resource---resourceId--attachable--field-" data-component="url" required  hidden>
<br>
</p>
</form>


## List the available morphable resources for a given resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova-api/ad/morphable/officia" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/ad/morphable/officia"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova-api--resource--morphable--field-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova-api--resource--morphable--field-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova-api--resource--morphable--field-"></code></pre>
</div>
<div id="execution-error-GETnova-api--resource--morphable--field-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova-api--resource--morphable--field-"></code></pre>
</div>
<form id="form-GETnova-api--resource--morphable--field-" data-method="GET" data-path="nova-api/{resource}/morphable/{field}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova-api--resource--morphable--field-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova-api--resource--morphable--field-" onclick="tryItOut('GETnova-api--resource--morphable--field-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova-api--resource--morphable--field-" onclick="cancelTryOut('GETnova-api--resource--morphable--field-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova-api--resource--morphable--field-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova-api/{resource}/morphable/{field}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="GETnova-api--resource--morphable--field-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>field</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="field" data-endpoint="GETnova-api--resource--morphable--field-" data-component="url" required  hidden>
<br>
</p>
</form>


## Attach a related resource to the given resource.




> Example request:

```bash
curl -X POST \
    "http://localhost/nova-api/deleniti/et/attach/enim" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/deleniti/et/attach/enim"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTnova-api--resource---resourceId--attach--relatedResource-" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTnova-api--resource---resourceId--attach--relatedResource-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTnova-api--resource---resourceId--attach--relatedResource-"></code></pre>
</div>
<div id="execution-error-POSTnova-api--resource---resourceId--attach--relatedResource-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTnova-api--resource---resourceId--attach--relatedResource-"></code></pre>
</div>
<form id="form-POSTnova-api--resource---resourceId--attach--relatedResource-" data-method="POST" data-path="nova-api/{resource}/{resourceId}/attach/{relatedResource}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTnova-api--resource---resourceId--attach--relatedResource-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTnova-api--resource---resourceId--attach--relatedResource-" onclick="tryItOut('POSTnova-api--resource---resourceId--attach--relatedResource-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTnova-api--resource---resourceId--attach--relatedResource-" onclick="cancelTryOut('POSTnova-api--resource---resourceId--attach--relatedResource-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTnova-api--resource---resourceId--attach--relatedResource-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>nova-api/{resource}/{resourceId}/attach/{relatedResource}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="POSTnova-api--resource---resourceId--attach--relatedResource-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="POSTnova-api--resource---resourceId--attach--relatedResource-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>relatedResource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="relatedResource" data-endpoint="POSTnova-api--resource---resourceId--attach--relatedResource-" data-component="url" required  hidden>
<br>
</p>
</form>


## Update an attached resource pivot record.




> Example request:

```bash
curl -X POST \
    "http://localhost/nova-api/necessitatibus/eum/update-attached/corporis/magni" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/necessitatibus/eum/update-attached/corporis/magni"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-"></code></pre>
</div>
<div id="execution-error-POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-"></code></pre>
</div>
<form id="form-POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-" data-method="POST" data-path="nova-api/{resource}/{resourceId}/update-attached/{relatedResource}/{relatedResourceId}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-" onclick="tryItOut('POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-" onclick="cancelTryOut('POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>nova-api/{resource}/{resourceId}/update-attached/{relatedResource}/{relatedResourceId}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>relatedResource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="relatedResource" data-endpoint="POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>relatedResourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="relatedResourceId" data-endpoint="POSTnova-api--resource---resourceId--update-attached--relatedResource---relatedResourceId-" data-component="url" required  hidden>
<br>
</p>
</form>


## Attach a related resource to the given resource.




> Example request:

```bash
curl -X POST \
    "http://localhost/nova-api/rem/voluptatem/attach-morphed/eos" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-api/rem/voluptatem/attach-morphed/eos"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTnova-api--resource---resourceId--attach-morphed--relatedResource-" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTnova-api--resource---resourceId--attach-morphed--relatedResource-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTnova-api--resource---resourceId--attach-morphed--relatedResource-"></code></pre>
</div>
<div id="execution-error-POSTnova-api--resource---resourceId--attach-morphed--relatedResource-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTnova-api--resource---resourceId--attach-morphed--relatedResource-"></code></pre>
</div>
<form id="form-POSTnova-api--resource---resourceId--attach-morphed--relatedResource-" data-method="POST" data-path="nova-api/{resource}/{resourceId}/attach-morphed/{relatedResource}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTnova-api--resource---resourceId--attach-morphed--relatedResource-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTnova-api--resource---resourceId--attach-morphed--relatedResource-" onclick="tryItOut('POSTnova-api--resource---resourceId--attach-morphed--relatedResource-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTnova-api--resource---resourceId--attach-morphed--relatedResource-" onclick="cancelTryOut('POSTnova-api--resource---resourceId--attach-morphed--relatedResource-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTnova-api--resource---resourceId--attach-morphed--relatedResource-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>nova-api/{resource}/{resourceId}/attach-morphed/{relatedResource}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>resource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resource" data-endpoint="POSTnova-api--resource---resourceId--attach-morphed--relatedResource-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>resourceId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="resourceId" data-endpoint="POSTnova-api--resource---resourceId--attach-morphed--relatedResource-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>relatedResource</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="relatedResource" data-endpoint="POSTnova-api--resource---resourceId--attach-morphed--relatedResource-" data-component="url" required  hidden>
<br>
</p>
</form>


## nova-vendor/nova-belongsto-depend




> Example request:

```bash
curl -X POST \
    "http://localhost/nova-vendor/nova-belongsto-depend" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova-vendor/nova-belongsto-depend"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTnova-vendor-nova-belongsto-depend" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTnova-vendor-nova-belongsto-depend"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTnova-vendor-nova-belongsto-depend"></code></pre>
</div>
<div id="execution-error-POSTnova-vendor-nova-belongsto-depend" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTnova-vendor-nova-belongsto-depend"></code></pre>
</div>
<form id="form-POSTnova-vendor-nova-belongsto-depend" data-method="POST" data-path="nova-vendor/nova-belongsto-depend" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTnova-vendor-nova-belongsto-depend', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTnova-vendor-nova-belongsto-depend" onclick="tryItOut('POSTnova-vendor-nova-belongsto-depend');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTnova-vendor-nova-belongsto-depend" onclick="cancelTryOut('POSTnova-vendor-nova-belongsto-depend');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTnova-vendor-nova-belongsto-depend" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>nova-vendor/nova-belongsto-depend</code></b>
</p>
</form>


## Display the Nova Vue router.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova"></code></pre>
</div>
<div id="execution-error-GETnova" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova"></code></pre>
</div>
<form id="form-GETnova" data-method="GET" data-path="nova" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova" onclick="tryItOut('GETnova');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova" onclick="cancelTryOut('GETnova');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova</code></b>
</p>
</form>


## Display the Nova Vue router.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/nova/nihil" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/nova/nihil"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETnova--view-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETnova--view-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETnova--view-"></code></pre>
</div>
<div id="execution-error-GETnova--view-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETnova--view-"></code></pre>
</div>
<form id="form-GETnova--view-" data-method="GET" data-path="nova/{view}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETnova--view-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETnova--view-" onclick="tryItOut('GETnova--view-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETnova--view-" onclick="cancelTryOut('GETnova--view-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETnova--view-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>nova/{view}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>view</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="view" data-endpoint="GETnova--view-" data-component="url" required  hidden>
<br>
</p>
</form>



