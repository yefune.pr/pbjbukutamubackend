# Keperluan

APIs untuk data Keperluan

## view data keperluan




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/viewkeperluan" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/viewkeperluan"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 1,
        "keperluan": "Keperluan Satu",
        "deskripsi": "Deskrpsi keperluan Satu",
        "bagian": "Bagian satu",
        "subbagian": "Subbagian satu",
        "email": "subbagiansatu@mail.com",
        "created_at": "2021-02-03 00:00:00",
        "updated_at": "2021-02-03 00:00:00"
    },
    {
        "id": 2,
        "keperluan": "keperluan dua",
        "deskripsi": "deskripsi keperluan dua",
        "bagian": "bagian dua",
        "subbagian": "subbagian dua",
        "email": "subbagiandua@mail.com",
        "created_at": "2021-02-03 00:00:00",
        "updated_at": "2021-02-03 00:00:00"
    }
]
```
<div id="execution-results-GETapi-viewkeperluan" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-viewkeperluan"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-viewkeperluan"></code></pre>
</div>
<div id="execution-error-GETapi-viewkeperluan" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-viewkeperluan"></code></pre>
</div>
<form id="form-GETapi-viewkeperluan" data-method="GET" data-path="api/viewkeperluan" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-viewkeperluan', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-viewkeperluan" onclick="tryItOut('GETapi-viewkeperluan');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-viewkeperluan" onclick="cancelTryOut('GETapi-viewkeperluan');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-viewkeperluan" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/viewkeperluan</code></b>
</p>
</form>



