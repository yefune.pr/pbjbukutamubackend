# Notulen

APIs untuk data Notulen

## tambah data notulen




> Example request:

```bash
curl -X POST \
    "http://localhost/api/addnotulen" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idkedatangan":"et","notulentext":"adipisci","notulenfoto":"quam"}'

```

```javascript
const url = new URL(
    "http://localhost/api/addnotulen"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idkedatangan": "et",
    "notulentext": "adipisci",
    "notulenfoto": "quam"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-addnotulen" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-addnotulen"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-addnotulen"></code></pre>
</div>
<div id="execution-error-POSTapi-addnotulen" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-addnotulen"></code></pre>
</div>
<form id="form-POSTapi-addnotulen" data-method="POST" data-path="api/addnotulen" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-addnotulen', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-addnotulen" onclick="tryItOut('POSTapi-addnotulen');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-addnotulen" onclick="cancelTryOut('POSTapi-addnotulen');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-addnotulen" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/addnotulen</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>idkedatangan</code></b>&nbsp;&nbsp;<small>numeric</small>     <i>optional</i> &nbsp;
<input type="text" name="idkedatangan" data-endpoint="POSTapi-addnotulen" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>notulentext</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="notulentext" data-endpoint="POSTapi-addnotulen" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>notulenfoto</code></b>&nbsp;&nbsp;<small>image</small>     <i>optional</i> &nbsp;
<input type="text" name="notulenfoto" data-endpoint="POSTapi-addnotulen" data-component="body"  hidden>
<br>
file required The image</p>

</form>



