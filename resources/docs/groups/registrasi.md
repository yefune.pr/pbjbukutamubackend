# Registrasi

APIs untuk data Registrasi

## tambah data registrasi




> Example request:

```bash
curl -X POST \
    "http://localhost/api/createregistrasi" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"nama":"quia","asalinstansi":"labore","nohp":"doloribus","email":"quod","idkeperluan":"ut","fotodiri":"quasi","fotoktp":"officiis"}'

```

```javascript
const url = new URL(
    "http://localhost/api/createregistrasi"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "nama": "quia",
    "asalinstansi": "labore",
    "nohp": "doloribus",
    "email": "quod",
    "idkeperluan": "ut",
    "fotodiri": "quasi",
    "fotoktp": "officiis"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-createregistrasi" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-createregistrasi"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-createregistrasi"></code></pre>
</div>
<div id="execution-error-POSTapi-createregistrasi" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-createregistrasi"></code></pre>
</div>
<form id="form-POSTapi-createregistrasi" data-method="POST" data-path="api/createregistrasi" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-createregistrasi', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-createregistrasi" onclick="tryItOut('POSTapi-createregistrasi');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-createregistrasi" onclick="cancelTryOut('POSTapi-createregistrasi');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-createregistrasi" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/createregistrasi</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>nama</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="nama" data-endpoint="POSTapi-createregistrasi" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>asalinstansi</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="asalinstansi" data-endpoint="POSTapi-createregistrasi" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>nohp</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="nohp" data-endpoint="POSTapi-createregistrasi" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-createregistrasi" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>idkeperluan</code></b>&nbsp;&nbsp;<small>numeric</small>  &nbsp;
<input type="text" name="idkeperluan" data-endpoint="POSTapi-createregistrasi" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>fotodiri</code></b>&nbsp;&nbsp;<small>image</small>     <i>optional</i> &nbsp;
<input type="text" name="fotodiri" data-endpoint="POSTapi-createregistrasi" data-component="body"  hidden>
<br>
file required The image</p>
<p>
<b><code>fotoktp</code></b>&nbsp;&nbsp;<small>image</small>     <i>optional</i> &nbsp;
<input type="text" name="fotoktp" data-endpoint="POSTapi-createregistrasi" data-component="body"  hidden>
<br>
file required The image</p>

</form>


## update data registrasi - checkout




> Example request:

```bash
curl -X POST \
    "http://localhost/api/checkoutregistrasi" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":"laudantium"}'

```

```javascript
const url = new URL(
    "http://localhost/api/checkoutregistrasi"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": "laudantium"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-checkoutregistrasi" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-checkoutregistrasi"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-checkoutregistrasi"></code></pre>
</div>
<div id="execution-error-POSTapi-checkoutregistrasi" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-checkoutregistrasi"></code></pre>
</div>
<form id="form-POSTapi-checkoutregistrasi" data-method="POST" data-path="api/checkoutregistrasi" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-checkoutregistrasi', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-checkoutregistrasi" onclick="tryItOut('POSTapi-checkoutregistrasi');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-checkoutregistrasi" onclick="cancelTryOut('POSTapi-checkoutregistrasi');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-checkoutregistrasi" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/checkoutregistrasi</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>numeric</small>  &nbsp;
<input type="text" name="id" data-endpoint="POSTapi-checkoutregistrasi" data-component="body" required  hidden>
<br>
</p>

</form>


## update data registrasi - pic signoff




> Example request:

```bash
curl -X POST \
    "http://localhost/api/updatepicregistrasi" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"id":"officiis","idpic":"est","signoffpic":"dolore"}'

```

```javascript
const url = new URL(
    "http://localhost/api/updatepicregistrasi"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "id": "officiis",
    "idpic": "est",
    "signoffpic": "dolore"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-updatepicregistrasi" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-updatepicregistrasi"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-updatepicregistrasi"></code></pre>
</div>
<div id="execution-error-POSTapi-updatepicregistrasi" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-updatepicregistrasi"></code></pre>
</div>
<form id="form-POSTapi-updatepicregistrasi" data-method="POST" data-path="api/updatepicregistrasi" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-updatepicregistrasi', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-updatepicregistrasi" onclick="tryItOut('POSTapi-updatepicregistrasi');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-updatepicregistrasi" onclick="cancelTryOut('POSTapi-updatepicregistrasi');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-updatepicregistrasi" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/updatepicregistrasi</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>numeric</small>  &nbsp;
<input type="text" name="id" data-endpoint="POSTapi-updatepicregistrasi" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>idpic</code></b>&nbsp;&nbsp;<small>numeric</small>  &nbsp;
<input type="text" name="idpic" data-endpoint="POSTapi-updatepicregistrasi" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>signoffpic</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="signoffpic" data-endpoint="POSTapi-updatepicregistrasi" data-component="body" required  hidden>
<br>
</p>

</form>



