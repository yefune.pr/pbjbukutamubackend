<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Datetime;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;
use Illuminate\Support\Facades\Storage;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Panel;
use App\Models\pic;
use App\Models\keperluan;

class updatepicnotulen extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\registrasi::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];
    
    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public static function label() {
        return 'Update PIC & Notulen';
    }
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable()->gridder([
                'labelSize' => 'w-1/2',
                'contentSize' => 'w-1/2',
                'panelSize' => 'w-1/2', // Use field as box in a Panel
            ]),
            new Panel('Data Diri', $this->datadiri()),
            new Panel('Keperluan', $this->datakeperluan()),
            HasMany::make('notulen'),
            Select::make('Status','checkoutstatus')
                ->options([
                    'Baru' => 'Baru',
                    'CheckIn' => 'Check In',
                    'CheckOut' => 'Check Out',
                    'DataKeliru' => 'Data Keliru',
                ])->readonly(function ($request) {
                    return $request->isUpdateOrUpdateAttachedRequest();
                })->hideFromIndex()->gridder([
                    'labelSize' => 'w-1/2',
                    'contentSize' => 'w-1/2',
                    'panelSize' => 'w-1/2', // Use field as box in a Panel
                ]),
            Datetime::make('Check In Time','checkintime')->hideFromIndex()
                ->readonly(function ($request) {
                    return $request->isUpdateOrUpdateAttachedRequest();
                })->gridder([
                    'labelSize' => 'w-1/2',
                    'contentSize' => 'w-1/2',
                    'panelSize' => 'w-1/2', // Use field as box in a Panel
                ]),
            Datetime::make('Check Out Time','checkouttime')->hideFromIndex()
                ->readonly(function ($request) {
                    return $request->isUpdateOrUpdateAttachedRequest();
                })->gridder([
                    'labelSize' => 'w-1/2',
                    'contentSize' => 'w-1/2',
                    'panelSize' => 'w-1/2', // Use field as box in a Panel
                ]),
        ];
    }

    protected function datadiri()
    {
        return [
            Text::make('Nama','nama')->readonly(function ($request) {
                return $request->isUpdateOrUpdateAttachedRequest();
            }),
            Text::make('Asal Instansi','asalinstansi')->readonly(function ($request) {
                return $request->isUpdateOrUpdateAttachedRequest();
            }),
            Text::make('No HP','nohp')->readonly(function ($request) {
                return $request->isUpdateOrUpdateAttachedRequest();
            })->hideFromIndex(),
            Text::make('Email','email')->readonly(function ($request) {
                return $request->isUpdateOrUpdateAttachedRequest();
            })->hideFromIndex(),
             Image::make('Foto Diri', 'fotodiri')
                ->disk('fotodiri')
                ->thumbnail(function ($value, $disk) {
                    return $value ? Storage::disk($disk)->url($value) : null;
                })->readonly(function ($request) {
                    return $request->isUpdateOrUpdateAttachedRequest();
                })->hideFromIndex()
                ->gridder([
                    'labelSize' => 'w-1/2',
                    'contentSize' => 'w-1/2',
                    'panelSize' => 'w-1/2', // Use field as box in a Panel
                ]),
            Image::make('Foto KTP', 'fotoktp')
                ->disk('fotoktp')
                ->thumbnail(function ($value, $disk) {
                    return $value ? Storage::disk($disk)->url($value) : null;
                })->readonly(function ($request) {
                    return $request->isUpdateOrUpdateAttachedRequest();
                })->hideFromIndex()
                ->gridder([
                    'labelSize' => 'w-1/2',
                    'contentSize' => 'w-1/2',
                    'panelSize' => 'w-1/2', // Use field as box in a Panel
                    'additionalLabelClasses' => 'text-center',
                ]),
        ];
    }

    protected function datakeperluan()
    {
        return [
            BelongsTo::make('Keperluan')->readonly(function ($request) {
                return $request->isUpdateOrUpdateAttachedRequest();
            }),
            BelongsTo::make('PIC'),
            Boolean::make('Sign Off PIC','signoffpic')->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\CheckOutStatus,
            new Filters\KeperluanFilter,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
