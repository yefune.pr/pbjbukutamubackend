<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Datetime;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;
use Illuminate\Support\Facades\Storage;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Panel;
use App\Models\pic;
use App\Models\keperluan;

class registrasi extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\registrasi::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];
    
    public static function label() {
        return 'Registrasi';
    }
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            new Panel('Data Diri', $this->datadiri()),
            new Panel('Keperluan', $this->datakeperluan()),
            new Panel('Check In - Check Out', $this->datacheck()),
            HasMany::make('notulen'),
            Date::make('Created At','created_at')->hideFromIndex()
            ->hideWhenUpdating(),
            Date::make('Updated At','updated_at')->hideFromIndex()
            ->hideWhenUpdating(),
        ];
    }

    protected function datadiri()
    {
        return [
            Text::make('Nama','nama')->readonly(function ($request) {
                return $request->isUpdateOrUpdateAttachedRequest();
            }),
            Text::make('Asal Instansi','asalinstansi')->readonly(function ($request) {
                return $request->isUpdateOrUpdateAttachedRequest();
            }),
            Text::make('No HP','nohp')->readonly(function ($request) {
                return $request->isUpdateOrUpdateAttachedRequest();
            }),
            Text::make('Email','email')->readonly(function ($request) {
                return $request->isUpdateOrUpdateAttachedRequest();
            }),
             Image::make('Foto Diri', 'fotodiri')
                ->disk('fotodiri')
                ->thumbnail(function ($value, $disk) {
                    return $value ? Storage::disk($disk)->url($value) : null;
                })->readonly(function ($request) {
                    return $request->isUpdateOrUpdateAttachedRequest();
                })->hideFromIndex()
                ->gridder([
                    'labelSize' => 'w-1/2',
                    'contentSize' => 'w-1/2',
                    'panelSize' => 'w-1/2', // Use field as box in a Panel
                ]),
            Image::make('Foto KTP', 'fotoktp')
                ->disk('fotoktp')
                ->thumbnail(function ($value, $disk) {
                    return $value ? Storage::disk($disk)->url($value) : null;
                })->readonly(function ($request) {
                    return $request->isUpdateOrUpdateAttachedRequest();
                })->hideFromIndex()
                ->gridder([
                    'labelSize' => 'w-1/2',
                    'contentSize' => 'w-1/2',
                    'panelSize' => 'w-1/2', // Use field as box in a Panel
                    'additionalLabelClasses' => 'text-center',
                ]),
        ];
    }

    protected function datakeperluan()
    {
        return [
            //Select::make('ID Keperluan','idkeperluan')->options(\App\Models\keperluan::pluck('keperluan', 'id'))->hideFromIndex(),
            //Select::make('ID PIC','idpic')->options(\App\Models\pic::pluck('nama', 'id'))->hideFromIndex(),
            BelongsTo::make('Keperluan'),
            BelongsTo::make('PIC'),
            Boolean::make('Sign Off PIC','signoffpic')->hideFromIndex(),
        ];
    }

    protected function datacheck()
    {
        return [
            Datetime::make('Check In Time','checkintime')->hideFromIndex()
                ->readonly(function ($request) {
                    return $request->isUpdateOrUpdateAttachedRequest();
                }),
            Datetime::make('Check Out Time','checkouttime')->hideFromIndex()
                ->readonly(function ($request) {
                    return $request->isUpdateOrUpdateAttachedRequest();
                }),
            Select::make('Status','checkoutstatus')
                ->options([
                    'Baru' => 'Baru',
                    'CheckIn' => 'Check In',
                    'CheckOut' => 'Check Out',
                    'DataKeliru' => 'Data Keliru',
                ]),
            Text::make('Card ID','cardid')->hideFromIndex()->hideFromIndex(),
            Select::make('Card ID Status','cardidstatus')->hideFromIndex()
                ->options([
                'Dipinjam' => 'Dipinjam',
                'Kembali' => 'Kembali',
                'Baru' => 'Baru',
            ]),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
