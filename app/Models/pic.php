<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pic extends Model
{
    use HasFactory;

    protected $table='pic';
    
    protected $fillable = [
        'nama',
        'jabatan',
        'bagian',
        'subbagian',
        'email',
        'created_at',
        'updated_at',
    ];
}
