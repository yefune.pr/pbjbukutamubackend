<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class keperluan extends Model
{
    use HasFactory;

    protected $table='keperluan';

    protected $fillable = [
        'keperluan',
        'deskripsi',
        'bagian',
        'subbagian',
        'email',
        'created_at',
        'updated_at',
    ];
}
