<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class notulen extends Model
{
    use HasFactory;

    protected $table='notulen';

    protected $fillable = [
        'idkedatangan',
        'notulentext',
        'notulenfoto',
        'created_at',
        'updated_at',
    ];

}
