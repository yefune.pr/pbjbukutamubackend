<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class registrasi extends Model
{
    use HasFactory;

    protected $table='registrasi';

    protected $fillable = [
        'nama',
        'asalinstansi',
        'nohp',
        'email',
        'idkeperluan',
        'fotodiri',
        'fotoktp',
        'idpic',
        'signoffpic',
        'checkintime',
        'checkouttime',
        'checkoutstatus',
        'cardid',
        'cardidstatus',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'checkintime' => 'datetime',
        'checkouttime' => 'datetime',
    ];

    public function pic()
    {
        return $this->belongsTo(pic::class, 'idpic', 'id');
    }

    public function keperluan()
    {
        return $this->belongsTo(keperluan::class, 'idkeperluan', 'id');
    }

    public function notulen()
    {
        return $this->hasMany(notulen::class, 'idkedatangan', 'id');
    }
}
