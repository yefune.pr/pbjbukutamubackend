<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\registrasi;
use DB;
use Auth;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\Gmail; 

/**
 * @group Registrasi
 *
 * APIs untuk data Registrasi
 */
class registrasiController extends Controller
{
    public function createselfie(Request $request){

        // $validator = Validator::make(
        //     $request->all(),
        //     [
        //         'fotodiri' => 'required',
        //     ]
        // );
        // if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 422);

        $FotoDiri = time().'.'.$request->file->getClientOriginalExtension();
        $originFileName = $request->file->getClientOriginalName();
        $request->file->move(public_path('storagediri'), $FotoDiri);

        $query = DB::transaction(function() use (
            $FotoDiri) {

            $data = DB::table('registrasi')->insert([
                'fotodiri' => $FotoDiri,
                'checkoutstatus' => 'Data Baru',
                'cardidstatus' => 'Data Baru',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $IDKedatangan = DB::getPdo()->lastInsertId();
            return ($IDKedatangan);          
          });

          $jsonobj = ["idkedatangan"=>$query,
                        "message"=>"success||Foto Diri berhasil disimpan"];

          return ($jsonobj);
    }

        /**
	 * tambah data registrasi
     * @bodyParam nama string required
     * @bodyParam asalinstansi string required
     * @bodyParam nohp string required
     * @bodyParam email string required
     * @bodyParam idkeperluan numeric required
     * @bodyParam fotodiri image file required The image
     * @bodyParam fotoktp image file required The image
	 */
    public function createktp(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'idkedatangan'=>'required',
                'fotoktp' => 'required',
            ]
        );
        if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 422);

        $IDKedatangan = $request->input('idkedatangan');
        $FotoKTP = time().'.'.$request->file->getClientOriginalExtension();
        $originFileName = $request->file->getClientOriginalName();
        $request->file->move(public_path('storagektp'), $FotoKTP);

        $query = DB::transaction(function() use (
            $IDKedatangan,
            $FotoKTP) {

                DB::table('registrasi')->where('idkedatangan',$IDKedatangan)->update([
                    'fotoktp' => $FotoKTP
                    ]);

            return ($IDKedatangan);          
          });

          $jsonobj = ["idkedatangan"=>$query,
                        "message"=>"success||Foto KTP berhasil disimpan"];

          return ($jsonobj);
    }

    /**
	 * tambah data registrasi
     * @bodyParam nama string required
     * @bodyParam asalinstansi string required
     * @bodyParam nohp string required
     * @bodyParam email string required
     * @bodyParam idkeperluan numeric required
     * @bodyParam fotodiri image file required The image
     * @bodyParam fotoktp image file required The image
	 */
    public function create(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'idkedatangan'=>'required',
                'nama' => 'required|max:255',
                'asalinstansi' => 'required|max:100',
                'nohp' => 'required|max:100',
                'email' => 'required|max:100',
                'idkeperluan' => 'required|numeric',
            ]
        );
        if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 422);
        
        $IDKedatangan = $request->input('idkedatangan');
        $Nama = $request->input('nama');
        $AsalInstansi = $request->input('asalinstansi');
        $NoHP = $request->input('nohp');
        $Email = $request->input('email');
        $IDKeperluan = $request->input('idkeperluan');

        $query = DB::transaction(function() use (
            $Nama,
            $AsalInstansi,
            $NoHP,
            $Email,
            $IDKeperluan,
            $IDKedatangan) {

            DB::table('registrasi')->where('id',$IDKedatangan)->update([
                    'nama' => $Nama,
                    'asalinstansi' => $AsalInstansi,
                    'nohp' => $NoHP,
                    'email' => $Email,
                    'idkeperluan' => $IDKeperluan,
                    'checkintime' => date('Y-m-d H:i:s'),
                    'checkoutstatus' => 'Check In'
            ]);
              
            $data = DB::table('notulen')->insert([
                'idkedatangan' => $IDKedatangan,
                'notulentext' => '',
                'notulenfoto' => '',
                'created_at' => date('Y-m-d H:i:s')
            ]);
            return ($IDKedatangan);          
          });

          $jsonobj = ["idkedatangan"=>$query,
                        "message"=>"success||data Registrasi berhasil disimpan"];

          return ($jsonobj);
    }

    /**
	 * update data registrasi - checkout
     * @bodyParam id numeric required
	 */
    public function checkout(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'idkedatangan' => 'required|numeric',
            ]
        );
        if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 422);
        $ID = $request->input('idkedatangan');

        $query = DB::transaction(function() use ($ID) {
            DB::table('registrasi')
            ->where('id',$ID)
            ->update(['checkouttime' => date('Y-m-d H:i:s'),
                        'checkoutstatus' => 'Check Out']);
        });
        return ($query) ? "error||Terjadi kesalahan, Checkout gagal" : "success||Checkout berhasil" ;
        
    }

    /**
	 * update data registrasi - pic signoff
     * @bodyParam id numeric required
     * @bodyParam idpic numeric required
     * @bodyParam signoffpic string required
	 */
    public function updatepic(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'id' => 'required|numeric',
                'idpic' => 'required|numeric',
                'signoffpic' => 'required',
            ]
        );
        if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 422);

        $ID = $request->input('id');
        $IDPIC = $request->input('idpic');
        $SignOffPIC = $request->input('signoffpic');

        $query = DB::transaction(function() use (
            $ID, 
            $IDPIC,
            $SignOffPIC) {
            DB::table('registrasi')
            ->where('id',$ID)
            ->update(['idpic' => $IDPIC, 'signoffpic'=> $SignOffPIC]);
        });
        return ($query) ? "error||Terjadi kesalahan, Data gagal diupdate" : "success||Data berhasil disimpan";
        
    }
}
