<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Validator;

use Illuminate\Http\Request;

/**
 * @group Notulen
 *
 * APIs untuk data Notulen
 */
class notulenController extends Controller
{
    /**
	 * tambah data notulen
     * @bodyParam idkedatangan numeric
     * @bodyParam notulentext string
     * @bodyParam notulenfoto image file required The image
	 */
    public function add(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'idkedatangan' => 'required|numeric',
                'notulentext' => 'required',
                'notulenfoto' => 'required',
            ]
        );
        if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 422);

        $IDKedatangan = $request->input('idkedatangan');
        $NotulenText = $request->input('notulentext');
        $NotulenFoto = $request->input('notulenfoto');

        $CekNotulen = DB::table('notulen')->where('idkedatangan', $IDKedatangan)->first();
        $query = DB::transaction(function() use (
            $IDKedatangan,
            $NotulenText,
            $NotulenFoto,
            $CekNotulen) {


            if(!empty($CekNotulen)) 
            {
                DB::table('notulen')->where('idkedatangan',$IDKedatangan)->update([
                    'notulentext' => $NotulenText,
                    'notulenfoto' => $NotulenFoto,
                    'updated_at' => date('Y-m-d H:i:s')
                    ]);
            }else{  
                $data = DB::table('notulen')->insert([
                    'idkedatangan' => $IDKedatangan,
                    'notulentext' => $NotulenText,
                    'notulenfoto' => $NotulenFoto,
                    'created_at' => date('Y-m-d H:i:s')
                ])
                ->lastInsertId();;
            }

          });
          return ($query) ? "error||Terjadi kesalahan, notulen gagal disimpan" : "success||notulen berhasil disimpan" ;
        
    }
}
