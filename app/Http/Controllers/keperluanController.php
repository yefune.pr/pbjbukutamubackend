<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

/**
 * @group Keperluan
 *
 * APIs untuk data Keperluan
 */
class keperluanController extends Controller
{
    /**
	 * view data keperluan
	 */
    public function view(){
        $Keperluan = DB::table('keperluan')->get(); 
        return json_decode($Keperluan);
    }
}
